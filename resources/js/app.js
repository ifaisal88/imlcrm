/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
require('vue-multiselect/dist/vue-multiselect.min.css');
window.Vue = require('vue').default;

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas)
// import Toast from "vue-toastification";

window.axios = require('axios');
window.moment = require('moment');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(IconsPlugin)
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('font-awesome-icon', FontAwesomeIcon);

// ======================================================= Employee Profile Components ======================================================//
Vue.component('employee-profile', require('./components/employee/profile/Profile.vue').default);
Vue.component('line-manager', require('./components/employee/LineManager.vue').default);
Vue.component('employee-insurance-claims', require('./components/employee/InsuranceClaim.vue').default);
Vue.component('employee-insurance-details', require('./components/employee/EmployeeInsurance.vue').default);
Vue.component('employee-attendance', require('./components/employee/EmployeeAttendance.vue').default);
Vue.component('employee-attendance-history', require('./components/employee/EmployeeAttendanceHistory.vue').default);
Vue.component('employee-leave', require('./components/employee/EmployeeLeave.vue').default);
Vue.component('employee-leave-personal', require('./components/employee/EmployeeLeavePersonal.vue').default);
Vue.component('notifications-main', require('./components/employee/Notifications.vue').default);
Vue.component('data-table', require('./components/DataTable.vue').default);
// ==========================================================================================================================================//


// ======================================================= Payroll Components ======================================================//
Vue.component('employee-payroll-generate', require('./components/payroll/PayRoll.vue').default);
Vue.component('employee-payroll-details', require('./components/payroll/PayRollDetails.vue').default);
Vue.component('employee-payroll-slip', require('./components/payroll/PayRollSlip.vue').default);
// ==================================================================================================================================//


// ======================================================= Setting Components ======================================================//
// =============================================== Insurance Components ==============================================//
Vue.component('isnurance-benefits-main', require('./components/settings/management/InsuranceBenefits.vue').default);
Vue.component('insurance-companies-main', require('./components/settings/management/InsuranceCompanies.vue').default);
Vue.component('insurance-categories-main', require('./components/settings/management/InsuranceCategories.vue').default);
Vue.component('insurance-providers-main', require('./components/settings/management/InsuranceProvider.vue').default);

// =============================================== Assets Components ==============================================//
Vue.component('asset-brands-main', require('./components/settings/management/AssetBrands.vue').default);
Vue.component('asset-conditions-main', require('./components/settings/management/AssetCondition.vue').default);
Vue.component('asset-types-main', require('./components/settings/management/AssetType.vue').default);
Vue.component('asset-items-main', require('./components/settings/management/AssetItems.vue').default);
Vue.component('asset-vendors-main', require('./components/settings/management/AssetVendors.vue').default);

// =============================================== Education Components ==============================================//
Vue.component('qualifications-main', require('./components/settings/management/Qualifications.vue').default);
Vue.component('institutes-main', require('./components/settings/management/Institutes.vue').default);

// =============================================== PayMents Components ==============================================//
Vue.component('salaryrules-main', require('./components/settings/management/SalaryRules.vue').default);
Vue.component('pay-groups-main', require('./components/settings/management/PayGroups.vue').default);
Vue.component('payment-types-main', require('./components/settings/management/PaymentTypes.vue').default);
Vue.component('allowances-main', require('./components/settings/management/Allowances.vue').default);
Vue.component('deductables-main', require('./components/settings/management/Deductables.vue').default);

// =============================================== Comapny Management Components ==============================================//
Vue.component('attendance-statuses-main', require('./components/settings/management/AttendanceStatuses.vue').default);
Vue.component('bank-details-main', require('./components/settings/management/BankDetails.vue').default);
Vue.component('cities-main', require('./components/settings/management/Cities.vue').default);
Vue.component('companies-main', require('./components/settings/management/Companies.vue').default);
Vue.component('company-accounts-main', require('./components/settings/management/CompanyAccounts.vue').default);
Vue.component('contract-types-main', require('./components/settings/management/ContractTypes.vue').default);
Vue.component('cost-centers-main', require('./components/settings/management/CostCenters.vue').default);
Vue.component('countries-main', require('./components/settings/management/Countries.vue').default);
Vue.component('departments-main', require('./components/settings/management/Departments.vue').default);
Vue.component('designations-main', require('./components/settings/management/Designations.vue').default);
Vue.component('document-statuses-main', require('./components/settings/management/DocumentStatus.vue').default);
Vue.component('document-types-main', require('./components/settings/management/DocumentTypes.vue').default);
Vue.component('categories-main', require('./components/settings/management/Categories.vue').default);
Vue.component('employee-statuses-main', require('./components/settings/management/EmployeeStatuses.vue').default);
Vue.component('event-types-main', require('./components/settings/management/EventTypes.vue').default);
Vue.component('event-details-main', require('./components/settings/management/EventDetails.vue').default);
Vue.component('employee-types-main', require('./components/settings/management/EmployeeType.vue').default);
Vue.component('gratuity-rules-main', require('./components/settings/management/GratuityRules.vue').default);
Vue.component('currencies-main', require('./components/settings/management/Currencies.vue').default);
Vue.component('job-shifts-main', require('./components/settings/management/JobShifts.vue').default);
Vue.component('leave-per-annums-main', require('./components/settings/management/LeavePerAnnums.vue').default);
Vue.component('leave-types-main', require('./components/settings/management/LeaveTypes.vue').default);
Vue.component('position-categories-main', require('./components/settings/management/PositionCategories.vue').default);
Vue.component('projects-main', require('./components/settings/management/Projects.vue').default);
Vue.component('relations-main', require('./components/settings/management/Relations.vue').default);
Vue.component('religions-main', require('./components/settings/management/Religions.vue').default);
Vue.component('routes-main', require('./components/settings/management/Routes.vue').default);
Vue.component('sponsors-main', require('./components/settings/management/Sponsors.vue').default);
Vue.component('sub-departments-main', require('./components/settings/management/SubDepartments.vue').default);
Vue.component('ticket-entitlements-main', require('./components/settings/management/TicketEntitlements.vue').default);
Vue.component('visa-statuses-main', require('./components/settings/management/VisaStatuses.vue').default);
Vue.component('roles-main', require('./components/settings/management/Roles.vue').default);
Vue.component('permissions-main', require('./components/settings/management/Permissions.vue').default);
Vue.component('events-display-main', require('./components/settings/management/EventDisplay.vue').default);
// ================================================================================================================================//
// ========================================================================================================================================//

Vue.component('ticketEntitlements-main', require('./components/settings/management/TicketEntitlements.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
*/

const app = new Vue({
    el: '#app',
});
