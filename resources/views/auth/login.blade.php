<!doctype html>
<html lang="en">

<head>
<title>:: IML HRMS ::</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/color_skins.css') }}">
</head>

<body class="theme-orange">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                    <div class="top">
                        <img src="{{ asset('assets/images/logo-white.svg') }}" alt="Lucid">
                    </div>
					<div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        <div class="body">
                            <form method="POST" action="{{ route('login') }}" class="form-auth-small">
                                @csrf
                                @error('email')
                                    <div class="alert alert-danger small">{{ $message }}</div>
                                @enderror

                                <!-- Email Address -->
                                <div>
                                    <label for="email" class="control-label sr-only">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" :value="old('email')" placeholder="Email" required autofocus>
                                </div>

                                <!-- Password -->
                                <div class="mt-4">
                                    <label for="password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" required autocomplete="password" placeholder="Password">
                                </div>

                                <div class="flex items-center justify-end mt-4">
                                    @if (Route::has('password.request'))
                                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                            {{ __('Forgot your password?') }}
                                        </a>
                                    @endif

                                    <x-button class="btn btn-primary btn-lg btn-block">
                                        {{ __('Login') }}
                                    </x-button>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>
</html>
