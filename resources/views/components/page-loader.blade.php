<div class="page-loader-wrapper">
    <div class="loader">
        {{-- <div class="m-t-30"><img src="{{ asset('assets/images/logo-icon.svg') }}" width="48" height="48" alt="IML HRMS"></div> --}}
        <div class="m-t-30"><img src="{{ asset('assets/images/logo-white.svg') }}" width="150" height="70" alt="IML HRMS"></div>
        <p>Please wait...</p>        
    </div>
</div>