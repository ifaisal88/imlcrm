@extends('layouts.templates')
@section('extra-css')

    <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
    <style>
        .hideDiv {
            display: none;
        }

    </style>
@endsection
@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                    class="fa fa-arrow-left"></i></a></h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Employees</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('linemanagers.read.index')}}">Line Manager</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="app">
                <line-manager></line-manager>
            </div>
        </div>
    </div>

@endsection

@section('extra-js')

    <script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
    <script src="{{ asset('assets/vendor/dropify/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function() {});

    </script>
@endsection
