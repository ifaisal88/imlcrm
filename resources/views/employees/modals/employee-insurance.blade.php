<form action="{{ route('employees.update.insuranceDetails', [$employee->employee_id]) }}" method="POST">
    @csrf
    <div class="modal fade" id="employee-insurance" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Insurance</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_provider_id">Insurance Providers</label>
                        <select name="insurance_provider_id" class="simple-select2 w-100">
                            <option value="">Insurance Providers</option>
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_company_id">Policy/Company Name</label>
                        <select name="insurance_company_id" class="simple-select2 w-100">
                            <option value="">Policy/Company Name</option>
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="policy_number">Policy Number</label>
                        <input type="text" name="policy_number" id="policy_number" class="form-control" placeholder="Policy Number">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_category_id">Insurance Category</label>
                        <select name="insurance_category_id" class="simple-select2 w-100">
                            <option value="">Insurance Category</option>
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="policy_contract_start_date">Policy Contract From</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="policy_contract_start_date" id="date_of_birth" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Policy Contract From">
                        </div>
                    </div>
                    <div class="form-group col-6">
                        <label for="policy_contract_end_date">Policy Contract To</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="policy_contract_end_date" id="date_of_birth" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Policy Contract To">
                        </div>
                    </div>
                    <div class="form-group col-6">
                        <label for="employee_insured_start_date">Employee Ensured From</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="employee_insured_start_date" id="date_of_birth" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Policy Contract From">
                        </div>
                    </div>
                    <div class="form-group col-6">
                        <label for="employee_insured_end_date">Employee Ensured To</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="employee_insured_end_date" id="date_of_birth" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Policy Contract To">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="annual_premium_per_year_rate">Annual Premium</label>
                        <input type="text" name="annual_premium_per_year_rate" id="annual_premium_per_year_rate" class="form-control" placeholder="Annual Premium">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="actual_rate">Actual Rate</label>
                        <input type="text" name="actual_rate" id="actual_rate" class="form-control" placeholder="Actual Rate">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="monthly_rate">Monthly Rate</label>
                        <input type="text" name="monthly_rate" id="monthly_rate" class="form-control" placeholder="Monthly Rate">
                    </div>
			    </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</form>