<form action="{{ route('employees.store.dependants', [$employee->employee_id]) }}" method="post" id="dependants" class="form-auth-small" enctype="multipart/form-data">
    @csrf
	<div class="modal fade" id="employee-dependants" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="title" id="defaultModalLabel">Employee Dependants</h4>
				</div>
				<div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 pull-left">
                            <label for="relation">Relation with Dependant</label>
                            <select name="relation_id" class="simple-select2 w-100 @error('relation_id') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($relations as $relation)
                                    <option value="{{ $relation->id }}" {{ $relation->id == old('relation_id', '' ) ? 'selected' : '' }}>{{ $relation->name }}</option>
                                @endforeach
                            </select>
                            @error('relation_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="col-md-6 pull-left">
                            <label for="country_id">Country</label>
                            <select name="country_id" class="simple-select2 w-100 @error('country_id') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ $country->id == old( 'country_id', '' ) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                            @error('country_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 pull-left">
                            <label for="name">Dependent Full Name</label>
                            <input name="name" id="name" type="text" value="{{ old('name', '') }}" class="form-control @error('name') is-invalid @enderror" placeholder="Dependant Full Name">
                            @error('name')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-5 pull-left">
                            <label for="country">Date of Birth</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                                </div>
                                <input name="date_of_birth" value="{{ old('date_of_birth', '') }}" data-provide="datepicker" data-date-autoclose="true" class="form-control @error('date_of_birth') is-invalid @enderror" data-date-format="yyyy-mm-dd" placeholder="Date of Birth">
                                @error('date_of_birth')
                                    <span class="text-danger"><small>{{ $message }}</small></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 pull-left">
                            <label for="passport_number">Passport Number</label>
                            <input name="passport_number" value="{{ old('passport_number', '') }}" id="passport_number" type="text" class="form-control @error('passport_number') is-invalid @enderror" placeholder="Passport Number">
                            @error('passport_number')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-5 pull-left">
                            <label for="passport_expiry">Expiry Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                                </div>
                                <input name="passport_expiry" value="{{ old('passport_expiry', '') }}" data-provide="datepicker" data-date-autoclose="true" class="form-control @error('passport_expiry') is-invalid @enderror" data-date-format="yyyy-mm-dd" placeholder="Expiry Date">
                                @error('passport_expiry')
                                    <span class="text-danger"><small>{{ $message }}</small></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 pull-left">
                            <label for="visa_number">Visa Number</label>
                            <input name="visa_number" value="{{ old('visa_number', '') }}" id="visa_number" type="text" class="form-control @error('visa_number') is-invalid @enderror" placeholder="Visa Number">
                            @error('visa_number')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-5 pull-left">
                            <label for="visa_expiry">Expiry Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                                </div>
                                <input name="visa_expiry" value="{{ old('visa_expiry', '') }}" id="visa_expiry" data-provide="datepicker" data-date-autoclose="true" class="form-control @error('email') is-invalid @enderror" data-date-format="yyyy-mm-dd" placeholder="Expiry Date">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="fancy-checkbox pull-left">
                                <label>
                                    <input name="travel" value="1" type="checkbox">
                                    <span>Travel Fare</span>
                                </label>
                            </div>
                            <div class="fancy-checkbox pull-left">
                                <label>
                                    <input name="insurance" value="1" type="checkbox">
                                    <span>Insurance</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 pull-left">
                            <label for="remarks">Remarks</label>
                            <textarea name="remarks" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>