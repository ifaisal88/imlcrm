<form action="{{ route('employees.store.educationDetail', [$employee->employee_id]) }}" method="POST" enctype='multipart/form-data'>
    @csrf
    <div class="modal fade" id="employee-education" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="smallModalLabel">Employee Education Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 pull-left">
                            <label for="qualification_id">Qualification</label>
                            <select name="qualification_id" id="qualification_id" class="simple-select2 w-100 @error('qualification_id') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($qualifications as $qualification)
                                    <option value="{{ $qualification->id }}" {{ $qualification->id == old( 'qualification_id', '' ) ? 'selected' : '' }}>{{ $qualification->name }}</option>
                                @endforeach
                            </select>
                            @error('qualification_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 pull-left">
                            <label for="institute_id">Institute</label>
                            <select name="institute_id" id="institute_id" class="simple-select2 w-100 @error('institute_id') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($institutes as $institute)
                                    <option value="{{ $institute->id }}" {{ $institute->id == old( 'institute_id', '' ) ? 'selected' : '' }}>{{ $institute->name }}</option>
                                @endforeach
                            </select>
                            @error('institute_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 pull-left">
                            <label for="country_id">Country</label>
                            <select name="country_id" id="country_id" class="simple-select2 w-100 @error('country_id') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ $country->id == old( 'country_id', '' ) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                            @error('country_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 pull-left">
                            <label for="marks">Marks/Grade</label>
                            <input type="text" value="{{ old('marks', '') }}" name="marks" id="marks" class="form-control @error('marks') is-invalid @enderror" placeholder="Marks/Grade">
                            @error('marks')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 pull-left">
                            <label for="completed_date">Date of Completion</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-calendar"></i></span>
                                </div>
                                <input value="{{ old('completed_date', '') }}" name="completed_date" id="completed_date" data-provide="datepicker" data-date-autoclose="true" class="form-control @error('completed_date') is-invalid @enderror" data-date-format="yyyy-mm-dd" placeholder="Date of Completion">
                                @error('completed_date')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <div class="fancy-checkbox pull-left">
                                <label>
                                    <input name="attested" value="{{ old('attested', '') }}" value="1" type="checkbox">
                                    <span>Attested</span>
                                </label>
                            </div>
                            <div class="fancy-checkbox pull-left">
                                <label>
                                    <input name="hardcopy" value="{{ old('hardcopy', '') }}" value="1" type="checkbox">
                                    <span>Hardcopy</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 pull-left">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror">{{ old('description', '') }}</textarea>
                            @error('description')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 pull-left">
                            <label for="degree_image">Degree Image to Upload</label>
                            <input type="file" name="degree" id="degree" class="dropify">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>