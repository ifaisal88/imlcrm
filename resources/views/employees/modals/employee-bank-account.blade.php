<form action="{{ route('employees.store.accountDetails', [$employee->employee_id]) }}" method="POST">
    @csrf
	<div class="modal fade" id="employee-bank-account" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="title" id="defaultModalLabel">Employee Bank Information</h4>
				</div>
				<div class="modal-body">
					<div class="form-group col-12 pull-left">
						<label for="bank_id">Bank Name</label>
						<select name="bank_id" class="simple-select2 w-100">
							<option value="">Bank Name</option>
                            @foreach($banks as $bank)
                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="account_no">Account Number</label>									
                        <input type="text" class="form-control" name="account_no" id="account_no" placeholder="Account Number">
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="iban_no">IBAN Number (23 Digits)</label>									
                        <input type="text" class="form-control" name="iban_no" id="iban_no" placeholder="IBAN Number">
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="account_holdar_name">Account Holdar Name</label>
                        <input type="text" class="form-control" name="account_holdar_name" id="account_holdar_name" placeholder="Account Holdar Name">
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>