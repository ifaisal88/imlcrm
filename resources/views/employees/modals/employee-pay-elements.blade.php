<form action="{{ route('employees.update.payElements', [$employee->employee_id]) }}" method="POST">
    @csrf
    @method('PUT')
	<div class="modal fade" id="employee-pay-elements" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="title" id="defaultModalLabel">Employee Pay Elements</h4>
				</div>
				<div class="modal-body">
					<div class="form-group col-4 pull-left">
						<label for="salary_rule_id">Salary Rule</label>
						<select name="salary_rule_id" class="simple-select2 w-100">
							<option value="">Salary Rule</option>
                            @foreach($relations as $relation)
                                <option value=""></option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-4 pull-left">
						<label for="salary_currency_id">Salary Currency ID</label>
						<select name="salary_currency_id" id="salary_currency_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($relations as $relation)
                                <option value=""></option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-4 pull-left">
						<label for="payment_type_id">Payment Type</label>
						<select name="payment_type_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($relations as $relation)
                                <option value=""></option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="last_salary_revision_date">Last Salary Revision Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="last_salary_revision_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Last Salary Revision Date">
                        </div>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="company_account_id">Company Account</label>
						<select name="company_account_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($relations as $relation)
                                <option value=""></option>
                            @endforeach
                        </select>
                    </div>
                    <h6>Salary Breakdown</h6>
                    <hr />
                    <div class="form-group col-4 pull-left">
                        <label for="basic_salary">Basic Salary</label>									
                        <input type="text" class="form-control" name="basic_salary" id="basic_salary" placeholder="Basic Salary">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="house_allounce">House Allounce</label>									
                        <input type="text" class="form-control" name="house_allounce" id="house_allounce" placeholder="House Allounce">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="insentives">Insentives</label>									
                        <input type="text" class="form-control" name="insentives" id="insentives" placeholder="Insentives">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="transport_allounce">Travel Allounce</label>									
                        <input type="text" class="form-control" name="transport_allounce" id="transport_allounce" placeholder="Transport Allounce">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="mobile_allounce">Mobile Allounce</label>									
                        <input type="text" class="form-control" name="mobile_allounce" id="mobile_allounce" placeholder="Mobile Allounce">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="other_allounce">Other Allounce</label>									
                        <input type="text" class="form-control" name="other_allounce" id="other_allounce" placeholder="Other Allounce">
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>