<form action="{{ route('employees.store.dependantBenefits', [$employee->employee_id]) }}" method="POST">
    @csrf
    <div class="modal fade" id="dependant-benefits" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Dependant Benefits</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-6 pull-left">
                        <label for="employee_dependant_id">Dependant Full Name</label>
                        <select name="employee_dependant_id" class="simple-select2 w-100">
                            <option value="">Dependant Full Name</option>
                            @foreach($employee_dependent_names as $employee_dependent_names)
                            <option value="{{ $employee_dependent_names->id }}">{{ $employee_dependent_names->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="ticket_entitlement_id">Ticket Entitlement</label>
                        <select name="ticket_entitlement_id" class="simple-select2 w-100">
                            <option value="">Ticket</option>
                            @foreach($ticket_entitlements as $ticket_entitlement)
                            <option value="{{ $ticket_entitlement->id }}">{{ $ticket_entitlement->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-7 pull-left">
                        <label for="routes_from_id">Route From</label>
                        <select name="routes_from_id" id="routes_from_id" class="simple-select2 w-100">
                            <option value="">Route From</option>
                            @foreach($routes as $route)
                            <option value="{{ $route->id }}">{{ $route->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-5 pull-left">
                        <label for="start">Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="start" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group col-7 pull-left">
                        <label for="routes_to_id">Route To</label>
                        <select name="routes_to_id" id="routes_to_id" class="simple-select2 w-100">
                            <option value="">Route To</option>
                            @foreach($routes as $route)
                            <option value="{{ $route->id }}">{{ $route->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-5 pull-left">
                        <label for="end">Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="end" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_company_id">Insurance Company</label>
                        <select name="insurance_company_id" class="simple-select2 w-100">
                            <option value="">Insurance Company</option>
                            @foreach($insurance_companies as $insurance_companie)
                            <option value="{{ $insurance_companie->id }}">{{ $insurance_companie->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_category_id">Insurance Category</label>
                        <select name="insurance_category_id" class="simple-select2 w-100">
                            <option value="">Insurance Category</option>
                            @foreach($insurance_categories as $insurance_categorie)
                            <option value="{{ $insurance_categorie->id }}">{{ $insurance_categorie->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="policy_number">Policy Number</label>
                        <input type="text" class="form-control" name="policy_number" id="policy_number" placeholder="Policy Number">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="currency_id">Currency</label>
                        <select name="currency_id" id="currency_id" class="simple-select2 w-100">
                            <option value="">Currency</option>
                            @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}">{{ $currency->symbol }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="fare_amount">Fare Amount</label>
                        <input type="text" class="form-control" name="fare_amount" id="fare_amount" placeholder="Fare Amount" value="">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="premium">Premium</label>
                        <input type="text" class="form-control" name="premium" id="premium" placeholder="Premium">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="insurance_amount">Insurance Amount</label>
                        <input type="text" class="form-control" name="insurance_amount" id="insurance_amount" placeholder="Insurance Amount">
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
			    </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</form>