<form action="{{ route('employees.store.contactPerson', [$employee->employee_id]) }}" method="POST">
    @csrf
    <div class="modal fade" id="emergency-contacts" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Emergency Contact Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-6 pull-left">
                        <label for="full_name">Full Name</label>
                        <input type="text" value="{{ old('full_name', '') }}" class="form-control @error('full_name') is-invalid @enderror" name="full_name" id="full_name" placeholder="Full Name" value="">
                        @error('full_name')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="email">Email</label>
                        <input type="text" value="{{ old('email', '') }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email">
                        @error('email')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-12 pull-left">
						<label for="relation">Relation</label>
						<select name="relation" class="simple-select2 w-100 @error('relation') is-invalid @enderror">
							<option value="">Relation</option>
                            @foreach($relations as $relation)
                                <option value="{{ $relation->id }}" {{ $relation->id == old( 'relation', '' ) ? 'selected' : '' }}>{{ $relation->name }}</option>
                            @endforeach
                        </select>
                        @error('relation')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="text" value="{{ old('mobile_number', '') }}" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" id="mobile_number" placeholder="Mobile Number">
                        @error('mobile_number')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="home_phone_number">Home Number</label>
                        <input type="text" value="{{ old('home_phone_number', '') }}" class="form-control @error('home_phone_number') is-invalid @enderror" name="home_phone_number" id="home_phone_number" placeholder="Home Number">
                        @error('home_phone_number')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="work_number">Work Number</label>
                        <input type="text" value="{{ old('work_number', '') }}" class="form-control @error('work_number') is-invalid @enderror" name="work_number" id="work_number" placeholder="Work Number">
                        @error('work_number')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="address">Address</label>
                        <textarea name="address" id="address" class="form-control @error('address') is-invalid @enderror">{{ old('address', '') }}</textarea>
                        @error('address')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror">{{ old('description', '') }}</textarea>
                        @error('description')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
			    </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
		    </div>
        </div>
    </div>
</form>