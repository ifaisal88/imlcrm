<form action="{{ route('employees.update.employeeRules', [$employee->employee_id]) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="modal fade" id="employee-rules" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Rules</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-6 pull-left">
                        <label for="contract_type_id">Contract Type</label>
                        <select name="contract_type_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($contractTypes as $contractType)
                            <option value="{{ $contractType->id }}">{{ $contractType->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="notice_period">Notice Period</label>
                        <select name="notice_period" class="simple-select2 w-100">
                            <option value="">Notice Period</option>
                            <option value="1 Month">1 Month</option>
                            <option value="2 Months">2 Months</option>
                            <option value="3 Months">3 Months</option>
                            <option value="4 Months">4 Months</option>
                            <option value="5 Months">5 Months</option>
                            <option value="6 Months">6 Months</option>
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="shift_id">Shift</label>
                        <select name="shift_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($shifts as $shift)
                                <option value="{{ $shift->id }}">{{ $shift->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="leave_per_annum_id">Leave Rule</label>
                        <select name="leave_per_annum_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($leaveRules as $leaveRule)
                                <option value="{{ $leaveRule->id }}">{{ $leaveRule->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="graduity_rule_country_id">Graduity Rule</label>
                        <select name="graduity_rule_country_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name . ' Rule' }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="ticket_entitlement_id">Ticket Entitlement</label>
                        <select name="ticket_entitlement_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($ticket_entitlements as $ticketEntitlement)
                                <option value="{{ $ticketEntitlement->id }}">{{ $ticketEntitlement->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="routes_from_id">Route From</label>
                        <select name="routes_from_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($routes as $route)
                                <option value="{{ $route->id }}">{{ $route->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="routes_to_id">Route To</label>
                        <select name="routes_to_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($routes as $route)
                                <option value="{{ $route->id }}">{{ $route->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="max_fair_rates">Max Fare Rates <small>(If Applicable)</small></label>
                        <input type="text" class="form-control" name="max_fair_rates" id="max_fair_rates" placeholder="Max Fare Rates" value="">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12">
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input type="checkbox" name="gratuity" value="1">
                                <span>Gratuity</span>
                            </label>
                        </div>
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input type="checkbox" name="air_fare" value="1">
                                <span>Air Fare</span>
                            </label>
                        </div>
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input type="checkbox" name="leave_salary" value="1">
                                <span>Leave Salary</span>
                            </label>
                        </div>
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input type="checkbox" name="medical_insurance" value="1">
                                <span>Medical Insurance</span>
                            </label>
                        </div>
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input type="checkbox" name="visa_processing_cost" value="1">
                                <span>Visa Processing Cost</span>
                            </label>
                        </div>
                    </div>
			    </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</form>