<form action="{{ route('employees.update.profileImage', [$employee->employee_id]) }}" method="POST" enctype='multipart/form-data'>
    @csrf
    @method('PUT')
    <div class="modal fade" id="profile-image" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="smallModalLabel">Profile Image</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-12 pull-left">
                        <label for="profile_image">Image to Upload</label>
                        <input type="file" required="required" name="profile_image" id="profile_image" data-default-file="{{ asset('storage/app/profile/images/' . $employee->profile_image) }}" class="dropify">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>