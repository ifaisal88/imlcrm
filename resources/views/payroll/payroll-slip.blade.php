@extends('layouts.templates')
@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                    lass="fa fa-arrow-left"></i></a> Employee PayRoll</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">payRoll</li>
                            <li class="breadcrumb-item active">Employee List</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card invoice1">
                        <div class="body">
                            <div class="invoice-top clearfix">
                                <div class="logo" style="height:50px;width:150px;">
                                    <img src="{{ asset('assets/images/logo.svg') }}" alt="company_logo"
                                        class="img-fluid" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="info" style="width:50%;">
                                    <h6>IML GROUP.</h6>
                                    <p>8117 Roosevelt St.<br />New Rochelle, NY 10801</p>
                                </div>
                                <div class="title">
                                    <h4>Invoice #{{ $salaryData['id'] }}</h4>
                                    <p>Salary Month: {{ $salaryData['salaryMonth'] }}</p>
                                </div>
                            </div>
                            <hr />
                            <div class="invoice-mid clearfix">
                                <div class="clientlogo">
                                    <img src="{{ $salaryData['profilePic'] }}" alt="user"
                                        class="rounded-circle img-fluid" />
                                </div>
                                <div class="info">
                                    <h6>{{ $salaryData['employeeName'] }}</h6>
                                    <p>
                                        {{ $salaryData['employeeDesignation'] }}<br />Employee ID:
                                        {{ $salaryData['employeeId'] }}
                                    </p>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead class="thead-success">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Allowances</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-dark">
                                                <tr>
                                                    <td>1</td>
                                                    <td>Basic Salary</td>
                                                    <td>{{ $salaryData['basicSalary'] }}</td>
                                                </tr>
                                                @foreach ($salaryData['allowances'] as $key => $allowance)
                                                    <tr>
                                                        <td>{{ $key + 2 }}</td>
                                                        <td>
                                                            {{ $allowance['name'] }}
                                                            @if (isset($allowance['remarks']))
                                                                <br>
                                                                ({{ $allowance['remarks'] }})
                                                            @endif
                                                        </td>
                                                        <td>{{ $allowance['amount'] }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    <td><strong>Total Earnings</strong></td>
                                                    <td><strong>{{ $salaryData['grossSalary'] }}</strong>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead class="thead-danger">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Deductions</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-dark">
                                                @foreach ($salaryData['deductables'] as $key => $deductable)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $deductable['name'] }}
                                                            @if (isset($deductable['remarks']))
                                                                <br>
                                                                ({{ $deductable['remarks'] }})
                                                            @endif
                                                        </td>
                                                        <td>{{ $deductable['amount'] }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    <td><strong>Total Deduction</strong></td>
                                                    <td><strong>{{ $salaryData['deductablesTotal'] }}</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <h5>Note</h5>
                                    <p>
                                        This is a computer generated slip, so no signature is required.
                                    </p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p class="m-b-0"><b>Total Earnings:</b>
                                        {{ $salaryData['grossSalary'] }}</p>
                                    <p class="m-b-0"><b>Total Deductions:</b> {{ $salaryData['deductablesTotal'] }}</p>
                                    <h5 class="m-b-0 m-t-10">Net Salary {{ $salaryData['netSalary'] }}</h5>
                                </div>
                                <div class="hidden-print col-md-12 text-right">
                                    <hr />
                                    <button class="btn btn-outline-secondary" id="btnExport">
                                        <i class="icon-printer"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('extra-js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js">
    </script>
    <script>
        $("#btnExport").on("click", function() {
            html2canvas($('.invoice1')[0], {
                onrendered: function(canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 550,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Table.pdf");
                }
            });
        });

    </script>

@endsection
