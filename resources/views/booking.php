<?php
if (isset($_POST['submit'])) {
    $to = "ifaisal88@gmail.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $visitor_name = $_POST['visitor_name'];
    $visitor_phone = $_POST['visitor_phone'];
    $visitor_date = $_POST['visitor_date'];
    $visitor_time = $_POST['visitor_time'];
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $visitor_name . " wrote the following:" . "\n\n" . $_POST['message'];
    // $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to, $subject, $message, $headers);
    mail($from, $subject2, $message2, $headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
}
?>
<form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input type="name" id="name" name="visitor_name" class="form-control form-control-lg border" placeholder="Full Name" pattern=[A-Z\sa-z]{3,20} required>
                </div>

                 <div class="form-group">
                  <input type="email" id="email" name="visitor_email" class="form-control form-control-lg border" placeholder="Email" required>
                </div>
				<div class="form-group">
                  <input type="phone" id="phone" name="visitor_phone" class="form-control form-control-lg border" placeholder="Phone" required>
                </div>

				<div class="form-row">
				 <div class="form-group col-6">
					  <div class="input-group date" id="datepicker">
						<input  id="date" class="form-control" name="visitor_date" placeholder="MM/DD/YYYY"/><span class="input-group-append input-group-addon">
						<span class="input-group-text"><i class="fa fa-calendar"></i></span></span>
					  </div>
					</div>


			  <div class="form-group col-6">
					  <div class="input-group time" id="timepicker">
						<input   class="form-control" id="time" name="visitor_time" placeholder="HH:MM AM/PM"/>
						<span class="input-group-append input-group-addon"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></span>
					  </div>
					</div>
		   </div>



                <div class="form-group">
				  <select class="custom-select form-control form-control-lg border" id="interestedin" name="interested_in" required>
					  <option selected>--- I am interested in ?---</option>
						<option value="Personal Life Coaching">Personal Life Coaching</option>
						<option value="Self-Esteem, Motivation & Personal Growth">Self-Esteem, Motivation & Personal Growth</option>
						<option value="Entrepreneurship, Career & Purpose"> Entrepreneurship, Career & Purpose </option>
						<option value="Relationship and Breakups">Relationship and Breakups</option>-->

					  </select>

				</div>
				<div class="form-group">

					  <select class="custom-select form-control form-control-lg border" id="call_type" name="call_type" required>
					  <option selected>--- Call Type ?---</option>
						<option value="Skype">Skype</option>
						<option value="Zoom Call">Zoom Call</option>
						<option value="Microsoft Teams">Microsoft Teams</option>
						<option value="Classic Phone Call"> Classic Phone Call </option>

					  </select>

				</div>
               <div class="form-group">
            	<button class="btn btn-lg  btn-dark rounded-pill btn-block btn-login" type="submit">Make a booking</button>
            </div>
                <div class="form-row">
                <div class="form-group col-md-6">
				<div class="g-recaptcha" data-sitekey="6Lf6oQAaAAAAABvlPlAavgSMXJgjt9VTHzfenv64"></div>
				</div>
				<div class="form-group col-md-6">
                <input type="text" data-validation="recaptcha" data-validation-recaptcha-sitekey="6Lf6oQAaAAAAABvlPlAavgSMXJgjt9VTHzfenv64" style="border:none; width:0; display:none;">
				</div></div>
              </form>
