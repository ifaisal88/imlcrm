<?php

return [
    'maritial_status' => [
        'Single',
        'Married',
        'Divorced',
        'Seprated'
    ],
    
    'blood_group' => [
        'A+',
        'A-',
        'B+',
        'B-',
        'AB+',
        'AB-',
        'O+',
        'O-',
        'Rh(null)'
    ],

    'employee_gender' => [
        'Male',
        'Female'
    ],

    'probation_period' => [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6'
    ]
];
