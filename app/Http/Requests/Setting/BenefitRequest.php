<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class BenefitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'insurance_category_id' => [
                'required',
                'integer'
            ],
            'name' => [
                'required',
                'string',
                Rule::unique('insurance_benefits')->ignore($this->insuranceBenefit)
            ],
            'code' => [
                'required',
                'string',
                Rule::unique('insurance_benefits')->ignore($this->insuranceBenefit)
            ],
            // 'amount' => [
            //     'nullable',
            //     'prohibited_if:percentage,true',
            // ],
            // 'percentage' => [
            //     'nullable',
            //     'prohibited_if:amount,true',
            // ],
            'description' => [
                'nullable',
                'string'
            ],
            'status' => [
                'nullable',
                'boolean'
            ]
        ];
    }
}
