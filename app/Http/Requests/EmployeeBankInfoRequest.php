<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeBankInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id' => 'required|numeric',
            'account_no' => 'required|numeric',
            'iban_no' => 'required|string',
            'account_holdar_name' => 'required|string',
            'description' => 'nullable|string|max:255'
        ];
    }

    public function attributes()
    {
        return [
            'bank_id' => 'bank',
            'account_holdar_name' => 'account holder'
        ];
    }
}
