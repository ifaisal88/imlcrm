<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class InsuranceClaimRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_employee_id' => [
                'required',
                'string',
            ],
            'total_amount' => [
                'required',
                'integer',
            ],
            'currency_id' => [
                'required',
                'integer',
            ],
            'description' => [
                'nullable',
                'string',
            ],
            'bill_date' => [
                'required',
                'date',
            ],
            'details' => [
                'required',
                'array'
            ]

        ];
    }

    public function messages()
    {
        return [
            'currency_id.required' => 'Select a currency.',
            'bill_date.required' => 'Date is requried.',
            'details.required' => 'No benefit added.',
            'details.array' => 'Invalid benefit details.',
        ];
    }
}
