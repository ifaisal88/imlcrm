<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeLeaveRequest extends FormRequest
{
    public $validator;

    public function __construct()
    {
        app('validator')->extend('date_range_check', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $startDate = $data['start_date'];
            $endDate = $data['end_date'];
            $id = [$data['id']];
            $employees = User::whereHas('employeeLeaves', function ($query) use ($startDate, $endDate, $id) {
                $query->where(function ($q) use ($startDate, $endDate, $id) {
                    $q->where(function ($q1) use ($startDate, $endDate) {
                        $q1->whereDate('start_date', '>=', $startDate)->whereDate('end_date', '<=', $endDate);
                    })->orWhere(function ($q2) use ($startDate, $endDate) {
                        $q2->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate);
                    })->orWhere(function ($q3) use ($startDate, $endDate) {
                        $q3->whereDate('start_date', '>', $startDate)->whereDate('start_date', '<', $endDate)->whereDate('end_date', '>', $endDate);
                    })->orWhere(function ($q4) use ($startDate, $endDate) {
                        $q4->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate)->whereDate('end_date', '<', $endDate);
                    });
                })->whereNotIn('id', [$id]);
            })->count();
            if ($employees > 0) {
                return false;
            } else {
                return true;
            }
        });
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => [
                'required',
                'string'
            ],
            'leave_type_id' => [
                'required',
                'integer'
            ],
            'start_date' => [
                'required',
                'date',
            ],
            'end_date' => [
                'required',
                'date',
                'after:start_date',
                'date_range_check'
            ],
            'is_paid' => [
                'nullable',
                'boolean'
            ],
            'approve' => [
                'nullable',
                'boolean'
            ],
            'remarks' => [
                'nullable',
                'string'
            ]
        ];
    }

    public function messages()
    {
        return [
            'end_date.date_range_check' => 'User leave request already exist in selected date range',
        ];
    }
}
