<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeDependentBenefitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_dependant_id' => 'required|numeric',
            'ticket_entitlement_id' => 'nullable|numeric',
            'routes_from_id'        => 'nullable|numeric',
            'routes_to_id'          => 'nullable|numeric|different:routes_from_id',
            'start'                 => 'nullable|date',
            'end'                   => 'nullable|date|after:start',
            'insurance_company_id'  => 'nullable|numeric',
            'insurance_category_id' => 'nullable|numeric',
            'currency_id'           => 'nullable|numeric',
            'fare_amount'           => 'nullable|numeric',
            'premium'               => 'nullable|numeric',
            'insurance_amount'      => 'nullable|numeric',
            'description'           => 'nullable|string|max:255',
        ];
    }

    public function attributes(){
        return [
            'routes_from_id' => 'Route From',
            'routes_to_id' => 'Route To',
            'start' => 'Departure Date',
            'end' => 'Return Date',
        ];
    }
}
