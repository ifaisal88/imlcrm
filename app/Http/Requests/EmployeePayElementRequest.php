<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeePayElementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salary_rule_id' => 'required|numeric',
            'salary_currency_id' => 'required|numeric',
            'payment_type_id' => 'required|numeric',
            'last_salary_revision_date' => 'nullable|date',
            'basic_salary' => 'required|numeric',
            // 'house_allounce' => 'required|numeric',
            // 'insentives' => 'required|numeric',
            // 'transport_allounce' => 'required|numeric',
            // 'mobile_allounce' => 'required|numeric',
            // 'other_allounce' => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'salary_rule_id' => 'salary rule',
            'salary_currency_id' => 'salary currency',
            'payment_type_id' => 'payment type',
        ];
    }
}
