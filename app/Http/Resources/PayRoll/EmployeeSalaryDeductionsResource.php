<?php

namespace App\Http\Resources\PayRoll;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeSalaryDeductionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (!is_null($this->deduction)) {
            return [
                'id' => $this->id,
                'deductionId' => $this->deduction->id,
                'name' => $this->deduction->name,
                'amount' => $this->amount,
                'remarks' => $this->remarks,
            ];
        }
    }
}
