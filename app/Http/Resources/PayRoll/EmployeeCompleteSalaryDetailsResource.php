<?php

namespace App\Http\Resources\PayRoll;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeCompleteSalaryDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
        ];
    }
}
