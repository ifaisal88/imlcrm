<?php

namespace App\Http\Resources\Employee;

use App\Models\EmployeeDependantBenefit;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Employee\Tabs\EmployeeAssetTabResource;
use App\Http\Resources\Employee\Tabs\EmployeeBankInfoTabResource;
use App\Http\Resources\Employee\Tabs\EmployeeDocumentTabResource;
use App\Http\Resources\Employee\Tabs\EmployeeDependentBenifitTabResource;
use App\Http\Resources\Employee\Tabs\PayElementAllowancesTabResource;

class UserOfficalTabsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'officalDetailsTab' => [
                'positionCategory' => [
                    'id' => isset($this->positionCategory->id) ? $this->positionCategory->id : '',
                    'name' => isset($this->positionCategory->name) ? $this->positionCategory->name : '',
                ],
                'designation' => [
                    'id' => isset($this->designation->id) ? $this->designation->id : '',
                    'name' => isset($this->designation->name) ? $this->designation->name : '',
                ],
                'department' => [
                    'id' => isset($this->department->id) ? $this->department->id : '',
                    'name' => isset($this->department->name) ? $this->department->name : '',
                ],
                'subDepartment' => [
                    'id' => isset($this->subDepartment->id) ? $this->subDepartment->id : '',
                    'name' => isset($this->subDepartment->name) ? $this->subDepartment->name : '',
                ],
                'country' => [
                    'id' => isset($this->country->id) ? $this->country->id : '',
                    'name' => isset($this->country->name) ? $this->country->name : '',
                ],
                'state' => [
                    'id' => isset($this->city->id) ? $this->city->id : '',
                    'name' => isset($this->city->name) ? $this->city->name : '',
                ],
                'project' => [
                    'id' => isset($this->project->id) ? $this->project->id : '',
                    'name' => isset($this->project->name) ? $this->project->name : '',
                ],
                'costCenter' => [
                    'id' => isset($this->costCenter->id) ? $this->costCenter->id : '',
                    'name' => isset($this->costCenter->name) ? $this->costCenter->name : '',
                ],
                'probationPeriod' => [
                    'id' => isset($this->probation_period) ? $this->probation_period : '',
                    'name' => isset($this->probation_period) ? $this->probation_period . ' Months' : '',
                ],
                'supervisor' => [
                    'id' => isset($this->supervisor) ? $this->supervisor : 0,
                    'name' => (isset($this->supervisor) && $this->supervisor == 1) ? 'Yes' : "No",
                ],
                'dateOfJoining' => isset($this->date_of_joining) ? $this->date_of_joining : '',
                'probationEnDate' => isset($this->probation_end_date) ? $this->probation_end_date : '',
                'exitDate' => isset($this->date_of_exit) ? $this->date_of_exit : '',
            ],
            'rulesTab' => [
                'contractType' => [
                    'id' => isset($this->contractType->id) ? $this->contractType->id : '',
                    'name' => isset($this->contractType->name) ? $this->contractType->name : '',
                ],
                'noticePeriod' => isset($this->notice_period) ? $this->notice_period : '',
                'shift' => [
                    'id' => isset($this->shift->id) ? $this->shift->id : '',
                    'name' => isset($this->shift->name) ? $this->shift->name : '',
                ],
                'leaveRule' => [
                    'id' => isset($this->leavePerAnnum->id) ? $this->leavePerAnnum->id : '',
                    'name' => isset($this->leavePerAnnum->name) ? $this->leavePerAnnum->name : '',
                ],
                'gratuityRule' => [
                    'id' => isset($this->graduityRuleCountry->id) ? $this->graduityRuleCountry->id : '',
                    'name' => isset($this->graduityRuleCountry->name) ? $this->graduityRuleCountry->name : '',
                ],
                'ticketEntitlement' => [
                    'id' => isset($this->ticketEntitlement->id) ? $this->ticketEntitlement->id : '',
                    'name' => isset($this->ticketEntitlement->name) ? $this->ticketEntitlement->name : '',
                ],
                'routeFrom' => [
                    'id' => isset($this->routeFrom->id) ? $this->routeFrom->id : '',
                    'name' => isset($this->routeFrom->name) ? $this->routeFrom->name : '',
                ],
                'routeTo' => [
                    'id' => isset($this->routeTo->id) ? $this->routeTo->id : '',
                    'name' => isset($this->routeTo->name) ? $this->routeTo->name : '',
                ],
                'maxFare' => isset($this->max_fair_rates) ? $this->max_fair_rates : '',
                'gratuity' => isset($this->gratuity) ? $this->gratuity : '',
                'airFare' => isset($this->air_fare) ? $this->air_fare : '',
                'leaveSalary' => isset($this->leave_salary) ? $this->leave_salary : '',
                'medicalInsurance' => isset($this->medical_insurance) ? $this->medical_insurance : '',
                'visaProcessingCost' => isset($this->visa_processing_cost) ? $this->visa_processing_cost : '',
            ],
            'dependentBenifitsTab' => EmployeeDependentBenifitTabResource::collection(isset($this->dependantBenefits) ? $this->dependantBenefits : []),
            'payElements' => [
                'salaryRule' => [
                    'id' => isset($this->salaryRule->id) ? $this->salaryRule->id : '',
                    'name' => isset($this->salaryRule->name) ? $this->salaryRule->name : '',
                ],
                'salaryCurrency' => [
                    'id' => isset($this->salaryCurrency->id) ? $this->salaryCurrency->id : '',
                    'name' => isset($this->salaryCurrency->name) ? $this->salaryCurrency->name : '',
                    'symbol' => isset($this->salaryCurrency->symbol) ? $this->salaryCurrency->symbol : '',
                ],
                'paymentType' => [
                    'id' => isset($this->paymentType->id) ? $this->paymentType->id : '',
                    'name' => isset($this->paymentType->name) ? $this->paymentType->name : '',
                ],
                'companyAccount' => [
                    'id' => isset($this->companyAccount->id) ? $this->companyAccount->id : '',
                    'name' => isset($this->companyAccount->name) ? $this->companyAccount->name : '',
                ],
                'basicSalary' => isset($this->basic_salary) ? $this->basic_salary : '',
                'employeeAllowances' => PayElementAllowancesTabResource::collection(isset($this->allowances) ? $this->allowances : []),
                'lastSalaryRevision' => isset($this->last_salary_revision_date) ? $this->last_salary_revision_date : '',
            ],
            'bankInfoTab' => EmployeeBankInfoTabResource::collection(isset($this->bankDetails) ? $this->bankDetails : []),
            'insuranceTab' => [
                'insuranceProvider' => [
                    'id' => isset($this->insuranceProvider->id) ? $this->insuranceProvider->id : '',
                    'name' => isset($this->insuranceProvider->name) ? $this->insuranceProvider->name : '',
                ],
                'category' => [
                    'id' => isset($this->insuranceCategory->id) ? $this->insuranceCategory->id : '',
                    'name' => isset($this->insuranceCategory->name) ? $this->insuranceCategory->name : '',
                ],
                'policyComapny' => [
                    'id' => isset($this->insuranceCompany->id) ? $this->insuranceCompany->id : '',
                    'name' => isset($this->insuranceCompany->name) ? $this->insuranceCompany->name : '',
                ],
                'policyNumber' => isset($this->policy_number) ? $this->policy_number : '',
                'policyContractStartDate' => isset($this->policy_contract_start_date) ? $this->policy_contract_start_date : '',
                'policyContractEndDate' => isset($this->policy_contract_end_date) ? $this->policy_contract_end_date : '',
                'employeeInsuredStartDate' => isset($this->employee_insured_start_date) ? $this->employee_insured_start_date : '',
                'employeeInsuredEndDate' => isset($this->employee_insured_end_date) ? $this->employee_insured_end_date : '',
                'annualPremiumPerYearRate' => isset($this->annual_premium_per_year_rate) ? $this->annual_premium_per_year_rate : '',
                'actualRate' => isset($this->actual_rate) ? $this->actual_rate : '',
                'monthlyRate' => isset($this->monthly_rate) ? $this->monthly_rate : '',
            ],
            'documentsTab' => EmployeeDocumentTabResource::collection(isset($this->documents) ? $this->documents : []),
            'assetsTab' => EmployeeAssetTabResource::collection(isset($this->assets) ? $this->assets : []),
        ];
        // return parent::toArray($request);
    }
}
