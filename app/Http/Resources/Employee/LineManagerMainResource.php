<?php

namespace App\Http\Resources\Employee;

use App\Http\Resources\Employee\Pages\LineManagerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LineManagerMainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return LineManagerResource::collection($this);
    }
}
