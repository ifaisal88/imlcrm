<?php

namespace App\Http\Resources\Employee;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeesDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->employee_id,
            'name' => [
                'name' => $this->full_name,
                'image' => $this->path,
            ],
            'employeeId' => $this->employee_id,
            'phone' => $this->home_phone_number,
            'joinDate' => $this->date_of_joining ? $this->date_of_joining : "N/A",
            'department' => $this->department ? $this->department->name : "N/A",
            'designation' => $this->designation ? $this->designation->name : "N/A",
            'action' => $this->employee_id,
        ];
    }
}
