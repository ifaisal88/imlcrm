<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class PayElementAllowancesTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->allowance->id,
            'name' => $this->allowance->name,
            'amount' => $this->amount,
        ];
    }
}
