<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'employeeId' => $this->employee->employee_id,
            'qualification' => [
                'id' => isset($this->qualification->id) ? $this->qualification->id : '',
                'name' => isset($this->qualification->name) ? $this->qualification->name : '',
            ],
            'institute' => [
                'id' => isset($this->institute->id) ? $this->institute->id : '',
                'name' => isset($this->institute->name) ? $this->institute->name : '',
            ],
            'country' => [
                'id' => isset($this->country->id) ? $this->country->id : '',
                'name' => isset($this->country->name) ? $this->country->name : '',
            ],
            'marks' => isset($this->marks) ? $this->marks : '',
            'year' => isset($this->completed_date) ? $this->completed_date : '',
            'attested' => ($this->attested) ? $this->attested : '',
            'hardcopy' => isset($this->hardcopy) ? $this->hardcopy : '',
            'document' => isset($this->degree_image) ? $this->degree_image : '',
            'documentPath' => $this->path,
        ];
    }
}
