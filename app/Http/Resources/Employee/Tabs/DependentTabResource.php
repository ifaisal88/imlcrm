<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class DependentTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'relation' => [
                'id' => isset($this->relation->id) ? $this->relation->id : '',
                'name' => isset($this->relation->name) ? $this->relation->name : '',
            ],
            'name' => isset($this->name) ? $this->name : '',
            'dateOfBirth' => isset($this->date_of_birth) ? $this->date_of_birth : '',
            'country' => [
                'id' => isset($this->country->id) ? $this->country->id : '',
                'name' => isset($this->country->name) ? $this->country->name : '',
            ],
            'passportNumber' => isset($this->passport_number) ? $this->passport_number : '',
            'passportExpiry' => isset($this->passport_expiry) ? $this->passport_expiry : '',
            'visaNumber' => isset($this->visa_number) ? $this->visa_number : '',
            'visaExpiry' => isset($this->visa_expiry) ? $this->visa_expiry : '',
            'travel' => isset($this->travel_allounce) ? $this->travel_allounce : '',
            'insurance' => isset($this->insurance) ? $this->insurance : '',
            'remarks' => isset($this->remarks) ? $this->remarks : '',
        ];
    }
}
