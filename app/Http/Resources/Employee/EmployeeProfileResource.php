<?php

namespace App\Http\Resources\Employee;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fullName' => $this->full_name,
            'firstName' => $this->first_name,
            'middleName' => $this->middle_name,
            'lastName' => $this->last_name,
            'employeeId' => $this->employee_id,
            'personalId' => $this->personal_id,
            'emiratesId' => $this->emirates_id,
            'profilePic' => $this->path,
            'laborCardNumber' => $this->labor_card_number,
            'personCode' => isset($this->person_code)?$this->person_code:'',
            'type' => [
                'id' => isset($this->employeeType->id) ? $this->employeeType->id : "",
                'name' => isset($this->employeeType->name) ? $this->employeeType->name : "",
            ],
            'visaStatus' => [
                'id' => isset($this->visaStatus->id) ? $this->visaStatus->id : "",
                'name' => isset($this->visaStatus->name) ? $this->visaStatus->name : "",
            ],
            'category' => [
                'id' => isset($this->employeeCategory->id) ? $this->employeeCategory->id : "",
                'name' => isset($this->employeeCategory->name) ? $this->employeeCategory->name : "",
            ],
            'status' => [
                'id' => isset($this->employeeStatus->id) ? $this->employeeStatus->id : "",
                'name' => isset($this->employeeStatus->name) ? $this->employeeStatus->name : "",
            ]
        ];
        // return parent::toArray($request);
    }
}
