<?php

namespace App\Http\Middleware;

use App\Models\Module;
use Closure;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $url = $request->route()->getName();
        $permissionCheck = explode(".", $url)[1];
        $module = Module::whereHas('urls', function ($query) use ($url) {
            $query->where('name', $url);
        })->first();
        $modules = session()->get('modules');
        $permissions = session()->get('permissions');
        if (!is_null($module) && in_array($module->slug, $modules)) {
            if (in_array($permissionCheck, $permissions[$module->name])) {
                return $next($request);
            } else {
                return response()->view('components.page-403');
            }
        } else {
            return response()->view('components.page-403');
        }
    }
}
