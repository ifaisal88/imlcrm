<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\BenefitRequest;
use Illuminate\Http\Request;
use App\Models\InsuranceBenefit;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class InsuranceBenefitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $benefits = InsuranceBenefit::with('insuranceCategory')->get();
        return response()->json($benefits);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BenefitRequest $request)
    {
        try{
            InsuranceBenefit::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InsuranceBenefits  $insuranceBenefits
     * @return \Illuminate\Http\Response
     */
    public function show(InsuranceBenefits $insuranceBenefits)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InsuranceBenefits  $insuranceBenefits
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceBenefit $insuranceBenefits)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InsuranceBenefit  $insuranceBenefits
     * @return \Illuminate\Http\Response
     */
    public function update(BenefitRequest $request, InsuranceBenefit $insuranceBenefit)
    {
        try{
            $insuranceBenefit->update($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InsuranceBenefits  $insuranceBenefits
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsuranceBenefit $insuranceBenefits)
    {
        //
    }
}
