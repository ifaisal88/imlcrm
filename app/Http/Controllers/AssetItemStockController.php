<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\AssetItemStockRequest;
use App\Models\AssetItemStock;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetItemStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetItemStockRequest $request)
    {
        try{
            AssetItemStock::create($request->all());
            return response()->json(['sueccess' => 'Record added successfully.']);
        }catch(\Exception $e){
            // throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetItemStock  $assetItemStock
     * @return \Illuminate\Http\Response
     */
    public function show(AssetItemStock $assetItemStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetItemStock  $assetItemStock
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetItemStock $assetItemStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetItemStock  $assetItemStock
     * @return \Illuminate\Http\Response
     */
    public function update(AssetItemStockRequest $request, AssetItemStock $assetItemStock)
    {
        try{
            $assetItemStock->update($request->all());
            return response()->json(['sueccess' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
            // throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetItemStock  $assetItemStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetItemStock $assetItemStock)
    {
        //
    }
}
