<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeBankDetail;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Http\Requests\EmployeeBankInfoRequest;

class EmployeeBankDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeBankDetail  $employeeBankDetail
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeBankDetail $employeeBankDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeBankDetail  $employeeBankDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeBankDetail $employeeBankDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeBankDetail  $employeeBankDetail
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeBankInfoRequest $request, EmployeeBankDetail $employeeBankDetail)
    {
        try{
            $employeeBankDetail->update($request->all());
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Bank infromation updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeBankDetail  $employeeBankDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeBankDetail $employeeBankDetail)
    {
        //
    }
}
