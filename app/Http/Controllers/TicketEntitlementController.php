<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\TicketEntitlementRequest;
use App\Models\TicketEntitlement;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class TicketEntitlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.ticketEntitlements.ticketEntitlements');
    }

    public function allTicketEntitlements()
    {
        $entitlments = TicketEntitlement::all();
        return response()->json($entitlments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketEntitlementRequest $request)
    {
        try {
            TicketEntitlement::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unkonw Error! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['succes' => 'Record Added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function show(ticketEntitlement $ticketEntitlement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function edit(ticketEntitlement $ticketEntitlement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function update(TicketEntitlementRequest $request, TicketEntitlement  $ticketEntitlement)
    {
        try {
            $ticketEntitlement->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unkonw Error! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['succes' => 'Record Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ticketEntitlement $ticketEntitlement)
    {
        //
    }
}
