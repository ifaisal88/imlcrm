<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeAssetRequest;
use Illuminate\Http\Request;
use App\Models\EmployeeAsset;
use App\Models\EmployeeAssetImage;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;

class EmployeeAssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAsset  $employeeAsset
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAsset $employeeAsset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAsset  $employeeAsset
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAsset $employeeAsset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAsset  $employeeAsset
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeAssetRequest $request, EmployeeAsset $employeeAsset)
    {
        try {
            $images = [];
            if (!is_null($request->assign_images)) {
                foreach ($request->assign_images as $key => $image) {
                    $content = file_get_contents($image['path']);
                    $ext = explode(".", $image['name']);
                    $name = str_replace(" ", "-", strtolower($employeeAsset->assetItem->name . ($key + 1) . "-assign." . $ext[1]));
                    $path = $employeeAsset->id . '/' . $name;
                    if (!Storage::disk('media_assets')->has($employeeAsset->id .'/'.$name)) {
                        Storage::disk('media_assets')->put($path, $content);
                        $images[] = [
                            'employee_asset_id' => $employeeAsset->id,
                            'image' => $name,
                            'type' => 'assigning'
                        ];
                    }
                }
            }
            if (!is_null($request->return_images)) {
                foreach ($request->return_images as $key => $image) {
                    $content = file_get_contents($image['path']);
                    $ext = explode(".", $image['name']);
                    $name = str_replace(" ", "-", strtolower($employeeAsset->assetItem->name . ($key + 1) . "-return." . $ext[1]));
                    $path = $employeeAsset->id . '/' . $name;
                    if (!Storage::disk('media_assets')->has($employeeAsset->id.'/'.$name)) {
                        Storage::disk('media_assets')->put($path, $content);
                        $images[] = [
                            'employee_asset_id' => $employeeAsset->id,
                            'image' => $name,
                            'type' => 'return'
                        ];
                    }
                }
            }
            $employeeAsset->update($request->all());
            foreach ($images as $imageStore) {
                EmployeeAssetImage::create($imageStore);
            }
            return response()->json(['success' => 'Bank infromation updated.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAsset  $employeeAsset
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAsset $employeeAsset)
    {
        //
    }

    public function deleteImages(Request $request, EmployeeAsset $employeeAsset){
        
    }
}
