<?php

namespace App\Http\Controllers;

use App\Models\EmployeeStatus;
use App\Http\Requests\Setting\EmployeeStatusRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.employeeStatuses.employeeStatuses');
    }

    public function allStatues()
    {
        $statuses = EmployeeStatus::all();
        return response()->json($statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStatusRequest $request)
    {
        try {
            EmployeeStatus::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeStatus  $employeeStatus
     * @return \Illuminate\Http\Response
     */
    public function show(employeeStatus $employeeStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeStatus  $employeeStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeStatus $employeeStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeStatus  $employeeStatus
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeStatusRequest $request, employeeStatus $employeeStatus)
    {
        try {
            $employeeStatus->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeStatus  $employeeStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeStatus $employeeStatus)
    {
        //
    }
}
