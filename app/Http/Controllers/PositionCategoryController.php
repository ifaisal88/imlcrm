<?php

namespace App\Http\Controllers;

use App\Models\PositionCategory;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\PositionCategoryRequest;

class PositionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.positionCategories.positionCategories');
    }

    public function allCategories()
    {
        $categories = PositionCategory::all();
        return response()->json($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PositionCategoryRequest $request)
    {
        try {
            PositionCategory::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function show(positionCategory $positionCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(positionCategory $positionCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function update(PositionCategoryRequest $request, positionCategory $positionCategory)
    {
        try {
            $positionCategory->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(positionCategory $positionCategory)
    {
        //
    }
}
