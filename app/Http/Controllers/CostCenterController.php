<?php

namespace App\Http\Controllers;

use App\Models\CostCenter;
use App\Http\Requests\Setting\CostCenterRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.costCenters.costCenters');
    }

    public function allCostCenters()
    {
        $centers = CostCenter::all();
        return response()->json($centers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostCenterRequest $request)
    {
        try {
            CostCenter::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function show(costCenter $costCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(costCenter $costCenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(CostCenterRequest $request, costCenter $costCenter)
    {
        try {
            $costCenter->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(costCenter $costCenter)
    {
        //
    }
}
