<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeDependant;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\EmployeeDependantRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeDependantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function show(employeeDependant $employeeDependant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeDependant $employeeDependant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeDependantRequest $request, employeeDependant $employeeDependant)
    {
        try{
            $employeeDependant->update($request->all());
            return response()->json(['suucess' => 'Dependent updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeDependant $employeeDependant)
    {
        //
    }
}
