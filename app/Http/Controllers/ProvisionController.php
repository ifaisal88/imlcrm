<?php

namespace App\Http\Controllers;

use App\Models\Provision;
use Illuminate\Http\Request;

class ProvisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\provision  $provision
     * @return \Illuminate\Http\Response
     */
    public function show(provision $provision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\provision  $provision
     * @return \Illuminate\Http\Response
     */
    public function edit(provision $provision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\provision  $provision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, provision $provision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\provision  $provision
     * @return \Illuminate\Http\Response
     */
    public function destroy(provision $provision)
    {
        //
    }
}
