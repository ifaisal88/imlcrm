<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceProvider;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\InsuranceProviderRequest;

class InsuranceProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.insurance.insuranceProviders');
    }

    public function allInsuranceProvider()
    {
        $providers = InsuranceProvider::all();
        return response()->json($providers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceProviderRequest $request)
    {
        try{
            InsuranceProvider::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceProviderRequest $request, InsuranceProvider $insuranceProvider)
    {
        try{
            $insuranceProvider->update($request->all());
            return response()->json(['success' => 'Record udpated successfully']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
