<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Setting\AssetItemRequest;
use App\Models\AssetItem;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.assets.assetsItems');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allItems()
    {
        $items = AssetItem::with('type', 'condition', 'brand', 'currency', 'stock.vendor', 'allocation.employee')->get();
        return response()->json($items);
    }

    /**
     * Show the form for getting filttered items.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $data = [];
        $data['type'] = !is_null($request->type)?$request->type:null;
        $data['condition'] = !is_null($request->condition)?$request->condition:null;
        $data['brand'] = !is_null($request->brand)?$request->brand:null;
        $items = AssetItem::itemsFilter($data)->with('currency')->get();
        $items = $items->where('unassigned_item', '>', 0);
        return response()->json($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetItemRequest $request)
    {
        try{
            AssetItem::create($request->except('quantity'));
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssetItemRequest $request, AssetItem $assetItem)
    {
        try{
            $assetItem->update($request->all());
            return response()->json(['success' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
