<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\GratuityRuleRequest;
use App\Models\GratuityRule;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class GratuityRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.gratuityRules.gratuityRules');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allRules()
    {
        $rules = GratuityRule::all();
        return response()->json($rules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GratuityRuleRequest $request)
    {
        try{
            GratuityRule::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GratuityRule  $gratuityRule
     * @return \Illuminate\Http\Response
     */
    public function show(GratuityRule $gratuityRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GratuityRule  $gratuityRule
     * @return \Illuminate\Http\Response
     */
    public function edit(GratuityRule $gratuityRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GratuityRule  $gratuityRule
     * @return \Illuminate\Http\Response
     */
    public function update(GratuityRuleRequest $request, GratuityRule $gratuityRule)
    {
        try{
            $gratuityRule->update($request->all());
            return response()->json(['success' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GratuityRule  $gratuityRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(GratuityRule $gratuityRule)
    {
        //
    }
}
