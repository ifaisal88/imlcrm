<?php

namespace App\Http\Controllers;

use App\Models\EmployeeType;
use App\Http\Requests\Setting\EmployeeTypeRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.employeeTypes.employeeTypes');
    }

    public function allTypes()
    {
        $types = EmployeeType::all();
        return response()->json($types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeTypeRequest $request)
    {
        try {
            EmployeeType::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function show(employeeType $employeeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeType $employeeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeTypeRequest $request, employeeType $employeeType)
    {
        try {
            $employeeType->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeType $employeeType)
    {
        //
    }
}
