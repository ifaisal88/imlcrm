<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmergencyContactRequest;
use Illuminate\Http\Request;
use App\Models\ContactPeople;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class ContactPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contactperson = ContactPeople::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function show(ContactPeople $contactPerson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactPeople $contactPerson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function update(EmergencyContactRequest $request, ContactPeople $contactPerson)
    {
        try{
            $contactPerson->update($request->all());
            return response()->json(['success' => 'Emergency contact updated.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactPeople $contactPerson)
    {
        //
    }
}
