<?php

namespace App\Http\Controllers;

use App\Exports\AttendanceHistoryExport;
use File;
use Input;

use App\Http\Resources\Employee\Attendance\AttendanceHistoryMain;
use App\Imports\AttendanceImport;
use Illuminate\Http\Request;
use App\Models\EmployeeAttendance;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;


class EmployeeAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $create = EmployeeAttendance::updateOrCreate(['id' => $request->id], $request->all(
                'employee_id',
                'attendance_status_id',
                'attendance_date',
                'checkin_time',
                'checkout_time',
                'remarks',
            ));
            return response()->json(['success' => 'Attendance Added.']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAttendance  $employeeAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAttendance $employeeAttendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAttendance  $employeeAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAttendance $employeeAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAttendance  $employeeAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAttendance $employeeAttendance)
    {
        try {
            $employeeAttendance->update($request->all(
                'attendance_status_id',
                'attendance_date',
                'checkin_time',
                'checkout_time',
            ));
            return response()->json(['success' => 'Attendance updated.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAttendance  $employeeAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAttendance $employeeAttendance)
    {
        //
    }

    public function import(Request $request)
    {
        $fileName = $request->file('document')->getClientOriginalName() ? $request->file('document')->getClientOriginalName() : null;
        $upload = Storage::disk('uploads')->put($fileName, File::get(Input::file('document')));
        $file = "upload" . DIRECTORY_SEPARATOR . $fileName;
        try {
            $import = Excel::import(new AttendanceImport, $file);
            return response()->json(['success' => 'Employees attendance added.']);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errors = [];
            foreach ($e->failures() as $failure) {
                $errors[] = $failure->values()['name'] . ' (' . $failure->values()['employee_id'] . ') ' . $failure->errors()[0];
            }
            throw new HttpResponseException(response($errors, Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function history(Request $request, $type = null)
    {
        $companyId = $request->company_id;
        $startDate = new Carbon($request->start_date);
        $endDate = new Carbon($request->end_date);

        $history = [];
        $employees = User::with(['employeeAttendance' => function ($query) use ($startDate, $endDate) {
            $query->whereDate('attendance_date', '>=', $startDate)->where('attendance_date', '<=', $endDate)->orderBy('attendance_date', 'ASC');
        }, 'employeeAttendance.attendanceStatus'])->where('company_id', $companyId)->get();

        $period = CarbonPeriod::create($request->start_date, $request->end_date);
        foreach ($employees as $employee) {
            $data = [
                'id' => $employee->employee_id,
                'name' => $employee->full_name,
                'emiratesId' => $employee->emirates_id,
                'department' => isset($employee->department->id)?$employee->department->id:"",
                'subDepartment' => isset($employee->subDepartment)?$employee->subDepartment->id:"",
            ];
            $attendances = $employee->employeeAttendance;
            foreach ($period as $date) {
                $attendance = !is_null($attendances) ? $attendances->where('attendance_date', $date->format('Y-m-d'))->first() : null;
                if (!is_null($attendance)) {
                    $data['attendances'][] = [
                        'date' => $date->format('Y-m-d'),
                        'checkInTime' => $attendance->checkin_time,
                        'checkOutTime' => $attendance->checkout_time,
                        'status' => $attendance->attendanceStatus->name,
                        'totalTime' => $attendance->total_time,
                        'available' => true
                    ];
                } else {
                    $data['attendances'][] = [
                        'date' => $date->format('Y-m-d'),
                        'checkInTime' => '',
                        'checkOutTime' => '',
                        'status' => '',
                        'totalTime' => '',
                        'available' => false
                    ];
                }
            }
            $history[] = $data;
        }
        if ($type == 'controller') {
            return $history;
        } else {
            return response()->json($history);
        }
    }

    public function export(Request $request)
    {
        $attendances = [];
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $companyId = $request->company_id;
        $employeesData = $this->history($request, 'controller');
        foreach ($employeesData as $employee) {
            $data = [];
            $data = [
                'name' => $employee['name'],
                'employee_id' => $employee['id'],
                'emirates_id' => $employee['emiratesId'],
            ];
            foreach ($employee['attendances'] as $attendance) {
                if ($attendance['available']) {
                    $data[$attendance['date']] = 'Status: ' . $attendance['status'] . ', Check In Time: ' . $attendance['checkInTime'] . ', Check Out Time: ' . $attendance['checkOutTime'].', Total Time:'. $attendance['totalTime'];
                } else {

                    $data[$attendance['date']] = '';
                }
            }
            $attendances[] = $data;
        }
        return Excel::download(new AttendanceHistoryExport($attendances, $startDate, $endDate), 'Attendance.xlsx');
    }
}
