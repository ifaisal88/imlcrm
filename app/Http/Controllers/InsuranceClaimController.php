<?php

namespace App\Http\Controllers;

use App\Http\Requests\InsuranceClaimRequest;
use App\Models\InsuranceClaim;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class InsuranceClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $claims = InsuranceClaim::with('details.benefit', 'currency')->get();
        return response()->json($claims);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceClaimRequest $request)
    {
        try {
            InsuranceClaim::create($request->all());
            return response()->json(['success' => 'Claim added successfully.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
            // throw new HttpResponseException(response("Unknown error occured! contact admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return \Illuminate\Http\Response
     */
    public function show(InsuranceClaim $insuranceClaim)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceClaim $insuranceClaim)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceClaimRequest $request, InsuranceClaim $insuranceClaim)
    {
        try {
            $insuranceClaim->update($request->all());
            return response()->json(['success' => 'Claim udpated successfully.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown error occured! contact admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsuranceClaim $insuranceClaim)
    {
        //
    }
}
