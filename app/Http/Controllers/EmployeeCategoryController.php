<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeCategory;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Setting\EmployeeCategoryRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('settings.categories.categories');
    }

    public function allCategories()
    {
        $categories = EmployeeCategory::all();
        return response()->json($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create(Request $request)
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeCategoryRequest $request)
    {
        try {
            EmployeeCategory::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeCategory $employeeCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeCategory $employeeCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeCategoryRequest $request, EmployeeCategory $employeeCategory)
    {
        try {
            $employeeCategory->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeCategory  $employeeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeCategory $employeeCategory)
    {
        //
    }
}
