<?php

namespace App\Http\Controllers;

use App\Models\VisaStatus;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\VisaStatusRequest;

class VisaStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.visaStatuses.visaStatuses');
    }

    public function allStatues()
    {
        $statuses = VisaStatus::all();
        return response()->json($statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisaStatusRequest $request)
    {
        try {
            VisaStatus::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function show(VisaStatus $visaStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(VisaStatus $visaStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function update(VisaStatusRequest $request, VisaStatus $visaStatus)
    {
        try {
            $visaStatus->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(VisaStatus $visaStatus)
    {
        //
    }
}
