<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\SalaryRuleRequest;
use App\Models\SalaryRule;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class SalaryRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.salaryRules.salaryRules');
    }

    public function allSalaryRule()
    {
        $rules = SalaryRule::all();
        return response()->json($rules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalaryRuleRequest $request)
    {
        try {
            $create = SalaryRule::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalaryRule  $salaryRule
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryRule $salaryRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalaryRule  $salaryRule
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryRule $salaryRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalaryRule  $salaryRule
     * @return \Illuminate\Http\Response
     */
    public function update(SalaryRuleRequest $request, SalaryRule $salaryRule)
    {
        try {
            $salaryRule->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalaryRule  $salaryRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryRule $salaryRule)
    {
        //
    }
}
