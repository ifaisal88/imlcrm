<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Sponsor;
use App\Models\Company;
use App\Models\PayGroup;
use Illuminate\Http\Request;
use App\Http\Requests\Setting\CompanyRequest;
use App\Models\AttendanceStatus;
use Carbon\CarbonPeriod;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.companies.companies');
    }

    public function allCompanies()
    {
        $companies = Company::with('country')->get();
        return response()->json($companies);
    }

    public function allPayGroups($id)
    {
        $paygroups = PayGroup::where('company_id', $id)->get();
        return response()->json($paygroups);
    }

    public function allSponsors($id)
    {
        $sponsors = Sponsor::where('company_id', $id)->get();
        return response()->json($sponsors);
    }

    public function employees(Company $company)
    {
        return response()->json($company->companyEmployees);
    }

    public function attendanceEmployees(Company $company, Request $request)
    {
        $employeesData = [];
        $period = CarbonPeriod::create($request->startDate, $request->endDate);
        $leaveStatus = AttendanceStatus::where('on_leave', true)->first();

        foreach ($period as $date) {

            $employees = User::with(['employeeAttendance' => function ($query) use ($date) {
                $query->where('attendance_date', $date);
            }, 'employeeLeaves' => function ($query) use ($date) {
                $query->whereDate('start_date', '<=', $date)->whereDate('end_date','>=', $date);
            }])->where('company_id', $company->id)->get();
            foreach ($employees as $employee) {
                $status = count($employee->employeeAttendance) > 0 ? $employee->employeeAttendance[0]->attendance_status_id : '';
                if (count($employee->employeeLeaves)) {
                    $status = $leaveStatus->id;
                }
                $employeesData[] = [
                    'value' => $employee->employee_id,
                    'text' => $employee->full_name,
                    'id' => $employee->employee_id,
                    'name' => $employee->full_name,
                    'emiratesId' => $employee->emirates_id,
                    'attendanceId' => count($employee->employeeAttendance) > 0 ? $employee->employeeAttendance[0]->id : '',
                    'department' => $employee->department_id,
                    'subDepartment' => $employee->sub_department_id,
                    'attendanceStatus' => $status,
                    'date' => $date->format('Y-m-d'),
                    'status' => false,
                    'checkInTime' => count($employee->employeeAttendance) > 0 ? $employee->employeeAttendance[0]->checkin_time : "",
                    'checkOutTime' => count($employee->employeeAttendance) > 0 ? $employee->employeeAttendance[0]->checkout_time : "",
                    'remarks' => count($employee->employeeAttendance) > 0 ?$employee->employeeAttendance[0]->remarks:"",
                    'disabled' => count($employee->employeeAttendance) > 0 ? true : false,
                ];
            }
        }

        return response()->json($employeesData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        try {
            Company::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response('Unknown Error! Contact Admin.', Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        try {
            $company->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response('Unknown Error! Contact Admin.', Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Reocrd updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
