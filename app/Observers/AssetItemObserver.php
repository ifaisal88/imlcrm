<?php

namespace App\Observers;

use App\Models\AssetItem;
use App\Models\AssetItemStock;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\throwException;

class AssetItemObserver
{
    /**
     * Handle the AssetItem "creating" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function creating(AssetItem $assetItem)
    {
        //
    }
    /**
     * Handle the AssetItem "created" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function created(AssetItem $assetItem)
    {
        //
    }

    /**
     * Handle the AssetItem "updated" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function updated(AssetItem $assetItem)
    {
        //
    }

    /**
     * Handle the AssetItem "deleted" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function deleted(AssetItem $assetItem)
    {
        //
    }

    /**
     * Handle the AssetItem "restored" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function restored(AssetItem $assetItem)
    {
        //
    }

    /**
     * Handle the AssetItem "force deleted" event.
     *
     * @param  \App\Models\AssetItem  $assetItem
     * @return void
     */
    public function forceDeleted(AssetItem $assetItem)
    {
        //
    }
}
