<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeAssetImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_asset_id',
        'type',
        'image',
        'created_by',
        'updated_by',
    ];

    public function asset()
    {
        return $this->belongsTo(EmployeeAsset::class, 'employee_asset_id');
    }
}
