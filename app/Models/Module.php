<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }

    public function urls()
    {
        return $this->hasMany(Url::class);
    }
}
