<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsuranceClaimDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'insurance_claim_id',
        'insurance_benefit_id',
        'amount',
        'description',
        'status',
    ];

    public function claim()
    {
        return $this->belongsTo(InsuranceClaim::class);
    }

    public function benefit()
    {
        return $this->belongsTo(InsuranceBenefit::class, 'insurance_benefit_id');
    }
}
