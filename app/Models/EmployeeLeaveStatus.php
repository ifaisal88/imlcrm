<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeLeaveStatus extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_leave_id',
        'status',
        'pending_with_user_id',
        'approved_by_user_id',
        'submitted_date',
        'approved_date',
        'comments',
        'created_by',
        'updated_by',
    ];

    public function request()
    {
        return $this->belongsTo(EmployeeLeave::class);
    }

    public function pendingBy()
    {
        return $this->belongsTo(User::class, 'pending_with_user_id');
    }

    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by_user_id');
    }
}
