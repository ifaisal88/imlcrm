<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeAllowance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'allowance_id',
        'remarks',
        'type',
        'amount',
        'status',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function allowance()
    {
        return $this->belongsTo(Allowance::class);
    }
}
