<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'asset_type_id',
        'asset_brand_id',
        'asset_condition_id',
        'currency_id',
        'name', 
        'description',
        'purchase_price',
        'sale_price',
        'status',
        'created_by',
        'updated_by'
    ];
    protected $appends = ['total_item', 'assigned_item', 'unassigned_item'];

    public function type()
    {
        return $this->belongsTo(AssetType::class, 'asset_type_id');
    }

    public function condition()
    {
        return $this->belongsTo(AssetCondition::class, 'asset_condition_id');
    }

    public function brand()
    {
        return $this->belongsTo(AssetBrand::class, 'asset_brand_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function stock()
    {
        return $this->hasMany(AssetItemStock::class);
    }

    public function allocation()
    {
        return $this->hasMany(EmployeeAsset::class)->where('returned', false);
    }

    public function getTotalItemAttribute()
    {
        return $this->stock->sum('quantity');
    }

    public function getAssignedItemAttribute()
    {
        return count($this->allocation);
    }

    public function getUnassignedItemAttribute()
    {
        return $this->total_item - $this->assigned_item;
    }

    public function scopeItemsFilter($query, $params)
    {
        $query = (new AssetItem)->newQuery();
        if (isset($params['type'])) {
            $query = $query->where('asset_type_id', $params['type']);
        }
        if (isset($params['condition'])) {
            $query = $query->where('asset_condition_id', $params['condition']);
        }
        if (isset($params['brand'])) {
            $query = $query->where('asset_brand_id', $params['brand']);
        }
        return $query;
    }
}
