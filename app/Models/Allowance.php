<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allowance extends Model
{

    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status',
        'enable_remarks',
        'slug',
        'default_enable',
        'created_by',
        'updated_by',
    ];

    public function scopeDefaultAllowances($query)
    {
        return $query->where('default_enable', true);
    }
}
