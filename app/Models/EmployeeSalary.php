<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EmployeeSalaryDetail;
use DB;

class EmployeeSalary extends Model
{
    use HasFactory;

    protected $appends = ['allowances_total', 'deductable_total'];
    protected $fillable = [
        'employee_id',
        'start_date',
        'end_date',
        'basic_salary',
        'gross_salary',
        'net_salary',
        'working_days',
        'total_days',
        'rate',
        'remarks'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id', 'employee_id');
    }

    public function getAllowancesTotalAttribute()
    {
        return $this->details->where('allowance_id', '!=', null)->sum('amount');
    }

    public function getDeductableTotalAttribute()
    {
        return $this->details->where('deduction_id', '!=', null)->sum('amount');
    }

    public function details()
    {
        return $this->hasMany(EmployeeSalaryDetail::class);
    }

    //================================== Static Boot Method =====================================//
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            DB::beginTransaction();
        });

        self::created(function ($model) {
            try {
                $details = [];
                foreach (request()->allowances as $allowance) {
                    $data = [];
                    $data['employee_id'] = request()->employee_id;
                    $data['employee_salary_id'] = $model->id;
                    $data['allowance_id'] = $allowance['id'];
                    $data['deduction_id'] = null;
                    $data['amount'] = $allowance['amount'];
                    $data['remarks'] = $allowance['remarks'];
                    $data['created_at'] = now();
                    $data['updated_at'] = now();
                    $details[] = $data;
                }
                foreach (request()->deducatbales as $deduction) {
                    $details[] = [
                        'employee_id' => request()->employee_id,
                        'employee_salary_id' => $model->id,
                        'deduction_id' => $deduction['id'],
                        'allowance_id' => null,
                        'amount' => $deduction['amount'],
                        'remarks' => $deduction['remarks'],
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
                $create = EmployeeSalaryDetail::insert($details);
                DB::commit();
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        });

        self::updating(function ($model) {
            DB::beginTransaction();
        });

        self::updated(function ($model) {
            try {
                foreach (request()->allowances as $allowance) {
                    EmployeeSalaryDetail::updateOrCreate(['id' => $allowance['id']], [
                        'employee_id' => request()->employee_id,
                        'employee_salary_id' => request()->salaryId,
                        'allowance_id' => $allowance['allowanceId'],
                        'name' => $allowance['name'],
                        'amount' => $allowance['amount']
                    ]);
                }
                foreach (request()->deductables as $deductable) {
                    EmployeeSalaryDetail::updateOrCreate(['id' => $deductable['id']], [
                        'employee_id' => request()->employee_id,
                        'employee_salary_id' => request()->salaryId,
                        'deduction_id' => $deductable['deductionId'],
                        'name' => $deductable['name'],
                        'amount' => $deductable['amount'],
                    ]);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception("Unknown Error! Contact Admin");
            }
        });
    }
}
