<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeePayElement extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'salary_rule',
        'currency_id',
        'basic',
        'house_allounce',
        'transport_allounce',
        'gross_salary',
        'last_salary_revision_date',
        'payment_type_id',
        'account_id',
        'description',
        'created_at',
        'updated_at'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }
}
