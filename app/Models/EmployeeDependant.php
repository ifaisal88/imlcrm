<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class EmployeeDependant extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_employee_id',
        'relation_id',
        'date_of_birth',
        'name',
        'country_id',
        'passport_number',
        'passport_expiry',
        'visa_number',
        'visa_expiry',
        'travel_allounce',
        'insurance',
        'remarks',
        'created_by',
        'updated_by',
    ];

    // public function setDateOfBirthAttribute($value)
    // {
    //     $this->attributes['date_of_birth'] = new Carbon($value);
    // }
    
    // public function setPassportExpiryAttribute($value)
    // {
    //     $this->attributes['passport_expiry'] = new Carbon($value);
    // }
    
    // public function setVisaExpiryAttribute($value)
    // {
    //     $this->attributes['visa_expiry'] = new Carbon($value);
    // }

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function relation(){
        return $this->belongsTo(Relation::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function employeeDependantBenefits()
    {
        return $this->hasMany(EmployeeDependantBenefit::class);
    }



    // public function dependantsName()
    // {
    //     return $this->hasMany(EmployeeDependantBenefit::class);
    // }
}
