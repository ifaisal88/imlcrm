<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDependantBenefit extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'ticket_entitlement_id',
        'employee_dependant_id',
        'routes_from_id',
        'routes_to_id',
        'fare_amount',
        'return_ticket',
        'insurance_company_id',
        'policy_number',
        'insurance_category_id',
        'premium',
        'insurance_amount',
        'start',
        'end',
        'status',
        'currency_id',
        'description',
        'created_by',
        'updated_by'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function employeeDependant()
    {
        return $this->belongsTo(EmployeeDependant::class);
    }

    public function ticketEntitlement()
    {
        return $this->belongsTo(TicketEntitlement::class);
    }

    public function routeTo()
    {
        return $this->belongsTo(Route::class, 'routes_to_id', 'id');
    }

    public function routeFrom()
    {
        return $this->belongsTo(Route::class, 'routes_from_id', 'id');
    }

    public function insuranceCompany()
    {
        return $this->belongsTo(InsuranceCompany::class);
    }

    public function insuranceCategory()
    {
        return $this->belongsTo(InsuranceCategory::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
