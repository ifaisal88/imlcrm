<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'status',
        'enable_remarks',
        'default_enable',
        'created_by',
        'updated_by',
    ];
}
