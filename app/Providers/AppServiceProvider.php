<?php

namespace App\Providers;

use App\Models\Allowance;
use App\Models\AssetItem;
use App\Models\EmployeeLeave;
use App\Models\InsuranceClaim;
use App\Models\User;
use App\Observers\AllowanceObserver;
use App\Observers\AssetItemObserver;
use App\Observers\EmployeeLeaveObserver;
use App\Observers\EmployeeObserver;
use App\Observers\InsuranceClaimObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(EmployeeObserver::class);
        Allowance::observe(AllowanceObserver::class);
        EmployeeLeave::observe(EmployeeLeaveObserver::class);
        InsuranceClaim::observe(InsuranceClaimObserver::class);
        AssetItem::observe(AssetItemObserver::class);
    }
}
