<?php

namespace App\Imports;

use App\Models\Company;
use App\Models\AttendanceStatus;
use App\Models\EmployeeAttendance;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
// use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Http\Exceptions\HttpResponseException;
// use Maatwebsite\Excel\Concerns\SkipsErrors;

class AttendanceImport implements ToModel, WithValidation, WithHeadingRow, SkipsOnError
{
    use Importable;
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        $company = Company::where('name', $row['company'])->first();
        $status = AttendanceStatus::where('name', $row['attendance_status'])->first();
        EmployeeAttendance::create([
            'employee_id' => $row['employee_id'],
            'company_id' => $company->id,
            'attendance_date' => $row['date'],
            'attendance_status_id' => !is_null($status) ? $status->id : '',
            'checkin_time' => $row['check_in_time'],
            'checkout_time' => $row['check_out_time'],
            'remarks' => $row['remarks']
        ]);
    }

    public function rules(): array
    {
        return [
            'attendance_status' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Attendance status is required');
                }
            },
            'employee_id' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Employee Id is required');
                }
            },
            'company' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Company is required');
                }
            },
            'date' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Date is required');
                }
            }
        ];
    }

    public function onError(\Throwable $e)
    {
        throw new \Exception($e);
    }
}
