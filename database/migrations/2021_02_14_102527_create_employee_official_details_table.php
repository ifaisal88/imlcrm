<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeOfficialDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_official_details', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('position_category_id');
            $table->unsignedBigInteger('designation_id');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('sub_department_id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('city_id');
            $table->date('date_of_joining');
            $table->integer('probition_period');
            $table->date('probition_end_date');
            $table->unsignedBigInteger('cost_center_id');
            $table->unsignedBigInteger('project_id');
            $table->boolean('is_supervisor');
            $table->date('exit_date');
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('position_category_id')
                  ->references('id')
                  ->on('position_categories');
            
            $table->foreign('designation_id')
                  ->references('id')
                  ->on('designations');

            $table->foreign('department_id')
                  ->references('id')
                  ->on('departments');

            $table->foreign('sub_department_id')
                  ->references('id')
                  ->on('sub_departments');

            $table->foreign('country_id')
                  ->references('id')
                  ->on('countries');

            $table->foreign('city_id')
                  ->references('id')
                  ->on('cities');

            $table->foreign('cost_center_id')
                  ->references('id')
                  ->on('cost_centers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_official_details');
    }
}
