<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->unique();
            $table->string('personal_id')->unique()->nullable();
            $table->string('emirates_id')->unique()->nullable();
            $table->string('labor_card_number')->unique()->nullable();
            $table->string('person_code')->unique()->nullable();
            $table->unsignedBigInteger('employee_type_id')->nullable()->default(null);
            $table->unsignedBigInteger('visa_status_id')->nullable()->default(null);
            $table->unsignedBigInteger('employee_category_id')->nullable()->default(null);
            $table->unsignedBigInteger('employee_status_id')->nullable()->default(null);
            $table->unsignedBigInteger('company_id')->nullable()->default(null);
            $table->unsignedBigInteger('sponsor_id')->nullable()->default(null);
            $table->unsignedBigInteger('pay_group_id')->nullable()->default(null);
            $table->unsignedBigInteger('nationality_id')->nullable()->default(null);
            $table->unsignedBigInteger('religion_id')->nullable()->default(null);
            $table->string('line_manager_id')->nullable()->default(null);
            $table->unsignedBigInteger('department_id')->nullable()->default(null);
            $table->unsignedBigInteger('sub_department_id')->nullable()->default(null);
            $table->unsignedBigInteger('designation_id')->nullable()->default(null);
            $table->unsignedBigInteger('leave_per_annum_id')->nullable()->default(null);
            $table->unsignedBigInteger('role_id')->nullable()->default(null);
            $table->unsignedBigInteger('contract_type_id')->nullable()->default(null);
            $table->unsignedBigInteger('position_category_id')->nullable()->default(null);
            $table->unsignedBigInteger('job_country_id')->nullable()->default(null);
            $table->unsignedBigInteger('job_city_id')->nullable()->default(null);
            $table->unsignedBigInteger('cost_center_id')->nullable()->default(null);
            $table->unsignedBigInteger('project_id')->nullable()->default(null);
            $table->unsignedBigInteger('shift_id')->nullable()->default(null);
            $table->unsignedBigInteger('gratuity_rule_country_id')->nullable()->default(null);
            $table->unsignedBigInteger('ticket_entitlement_id')->nullable()->default(null);
            $table->unsignedBigInteger('route_from_id')->nullable()->default(null);
            $table->unsignedBigInteger('route_to_id')->nullable()->default(null);
            $table->unsignedBigInteger('salary_rule_id')->nullable()->default(null);
            $table->unsignedBigInteger('payment_type_id')->nullable()->default(null);
            $table->unsignedBigInteger('salary_currency_id')->nullable()->default(null);
            $table->unsignedBigInteger('company_account_id')->nullable()->default(null);
            $table->unsignedBigInteger('insurance_provider_id')->nullable()->default(null);
            $table->unsignedBigInteger('insurance_company_id')->nullable()->default(null);
            $table->unsignedBigInteger('insurance_category_id')->nullable()->default(null);
            $table->string('maritial_status')->nullable()->default(null);
            $table->string('probation_period')->nullable()->default(null);
            $table->string('blood_group')->nullable()->default(null);
            $table->string('maiden_name')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->string('first_name');
            $table->string('middle_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('full_name');
            $table->string('parent_name')->nullable()->default(null);
            $table->string('email')->unique();
            $table->string('profile_image')->nullable()->default(null);
            $table->string('password');
            $table->string('personal_email')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);
            $table->string('home_phone_number')->nullable();
            $table->string('work_mobile')->nullable()->default(null);
            $table->string('current_address')->nullable()->default(null);
            $table->string('home_country_address')->nullable()->default(null);
            $table->string('notice_period')->nullable()->default(null);
            $table->date('date_of_birth')->nullable()->default(null);
            $table->date('date_of_joining')->nullable()->default(null);
            $table->date('probation_end_date')->nullable()->default(null);
            $table->date('date_of_exit')->nullable()->default(null);
            $table->date('last_salary_revision_date')->nullable()->default(null);
            $table->date('policy_contract_start_date')->nullable()->default(null);
            $table->date('policy_contract_end_date')->nullable()->default(null);
            $table->date('employee_insured_start_date')->nullable()->default(null);
            $table->date('employee_insured_end_date')->nullable()->default(null);
            $table->boolean('status')->default(true);
            $table->boolean('supervisor')->default(false);
            $table->boolean('gratuity')->default(false);
            $table->boolean('air_fare')->default(false);
            $table->boolean('leave_salary')->default(false);
            $table->boolean('visa_processing_cost')->default(false);
            $table->boolean('medical_insurance')->default(false);
            $table->float('fare_amount')->default(false);
            $table->float('basic_salary')->default(false);
            $table->float('house_allounce')->default(false);
            $table->float('insentives')->default(false);
            $table->float('transport_allounce')->default(false);
            $table->float('mobile_allounce')->default(false);
            $table->float('other_allounce')->default(false);
            $table->integer('policy_number')->default(false);
            $table->integer('annual_premium_per_year_rate')->default(false);
            $table->integer('monthly_rate')->default(false);
            $table->integer('actual_rate')->default(false);
            $table->integer('max_fair_rates')->nullable()->default(null);
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
