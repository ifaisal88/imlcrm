<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_assets', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('asset_type_id');
            $table->unsignedBigInteger('asset_brand_id');
            $table->unsignedBigInteger('asset_condition_id');
            $table->string('serial_no');
            $table->unsignedBigInteger('currency_id');
            $table->float('value', 8, 2);
            $table->string('description')->nullable()->default(null);
            $table->date('issue_date');
            $table->date('return_date')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('asset_type_id')
                  ->references('id')
                  ->on('asset_types');

            $table->foreign('asset_brand_id')
                  ->references('id')
                  ->on('asset_brands');

            $table->foreign('asset_condition_id')
                  ->references('id')
                  ->on('asset_conditions');

            $table->foreign('currency_id')
                  ->references('id')
                  ->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_assets');
    }
}
