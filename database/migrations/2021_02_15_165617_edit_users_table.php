<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('employee_type_id')
                ->references('id')
                ->on('employee_types');

            $table->foreign('visa_status_id')
                ->references('id')
                ->on('visa_statuses');

            $table->foreign('employee_category_id')
                ->references('id')
                ->on('employee_categories');

            $table->foreign('employee_status_id')
                ->references('id')
                ->on('employee_statuses');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies');

            $table->foreign('sponsor_id')
                ->references('id')
                ->on('sponsors');

            $table->foreign('pay_group_id')
                ->references('id')
                ->on('pay_groups');

            $table->foreign('nationality_id')
                ->references('id')
                ->on('countries');

            $table->foreign('religion_id')
                ->references('id')
                ->on('religions');

            $table->foreign('line_manager_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('department_id')
                ->references('id')
                ->on('departments');

            $table->foreign('sub_department_id')
                ->references('id')
                ->on('sub_departments');

            $table->foreign('designation_id')
                ->references('id')
                ->on('designations');

            $table->foreign('created_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles');

            $table->foreign('position_category_id')
                ->references('id')
                ->on('position_categories');

            $table->foreign('job_country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('job_city_id')
                ->references('id')
                ->on('cities');

            $table->foreign('cost_center_id')
                ->references('id')
                ->on('cost_centers');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects');

            $table->foreign('shift_id')
                ->references('id')
                ->on('shifts');

            $table->foreign('gratuity_rule_country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('ticket_entitlement_id')
                ->references('id')
                ->on('ticket_entitlements');

            $table->foreign('route_from_id')
                ->references('id')
                ->on('ticket_entitlements');

            $table->foreign('route_to_id')
                ->references('id')
                ->on('ticket_entitlements');

            $table->foreign('salary_rule_id')
                ->references('id')
                ->on('salary_rules');

            $table->foreign('payment_type_id')
                ->references('id')
                ->on('payment_types');

            $table->foreign('salary_currency_id')
                ->references('id')
                ->on('currencies');

            $table->foreign('company_account_id')
                ->references('id')
                ->on('accounts');

            $table->foreign('insurance_provider_id')
                ->references('id')
                ->on('insurance_providers');

            $table->foreign('insurance_company_id')
                ->references('id')
                ->on('insurance_companies');

            $table->foreign('insurance_category_id')
                ->references('id')
                ->on('insurance_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // Schema::dropIfExists('users');
            $table->dropForeign('users_employee_type_id_foreign');
            $table->dropForeign('users_visa_status_id_foreign');
            $table->dropForeign('users_employee_category_id_foreign');
            $table->dropForeign('users_employee_status_id_foreign');
            $table->dropForeign('users_company_id_foreign');
            $table->dropForeign('users_sponsor_id_foreign');
            $table->dropForeign('users_pay_group_id_foreign');
            $table->dropForeign('users_nationality_id_foreign');
            $table->dropForeign('users_religion_id_foreign');
            $table->dropForeign('users_line_manager_id_foreign');
            $table->dropForeign('users_department_id_foreign');
            $table->dropForeign('users_sub_department_id_foreign');
            $table->dropForeign('users_designation_id_foreign');
            $table->dropForeign('users_created_by_foreign');
            $table->dropForeign('users_updated_by_foreign');
            $table->dropForeign('users_role_id_foreign');
            $table->dropForeign('users_position_category_id_foreign');
            $table->dropForeign('users_job_country_id_foreign');
            $table->dropForeign('users_job_city_id_foreign');
            $table->dropForeign('users_cost_center_id_foreign');
            $table->dropForeign('users_project_id_foreign');
            $table->dropForeign('users_shift_id_foreign');
            $table->dropForeign('users_gratuity_rule_country_id_foreign');
            $table->dropForeign('users_ticket_entitlement_id_foreign');
            $table->dropForeign('users_route_from_id_foreign');
            $table->dropForeign('users_route_to_id_foreign');
            $table->dropForeign('users_salary_rule_id_foreign');
            $table->dropForeign('users_payment_type_id_foreign');
            $table->dropForeign('users_salary_currency_id_foreign');
            $table->dropForeign('users_company_account_id_foreign');
            $table->dropForeign('users_insurance_provider_id_foreign');
            $table->dropForeign('users_insurance_company_id_foreign');
            $table->dropForeign('users_insurance_category_id_foreign');
        });
    }
}