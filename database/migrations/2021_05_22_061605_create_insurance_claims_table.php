<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_claims', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->float('total_amount');
            $table->unsignedBigInteger('currency_id');
            $table->string('description')->nullable()->default(null);
            $table->date('bill_date');
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->foreign('user_employee_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_claims');
    }
}
