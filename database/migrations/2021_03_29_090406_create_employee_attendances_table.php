<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use function GuzzleHttp\default_ca_bundle;

class CreateEmployeeAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attendances', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->required();
            $table->unsignedBigInteger('attendance_status_id')->required();
            $table->date('attendance_date')->required();
            $table->time('checkin_time')->required();
            $table->time('checkout_time')->required();
            $table->string('remkarks')->nullable()->default(null);
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('attendance_status_id')
                  ->references('id')
                  ->on('attendance_statuses');

            $table->foreign('employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attendances');
    }
}
