<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_education', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('qualification_id');
            $table->unsignedBigInteger('institute_id');
            $table->unsignedBigInteger('country_id')->default(null);
            $table->float('marks', 8, 2)->nullable()->default(null);
            $table->date('completed_date')->nullable()->default(null);
            $table->boolean('attested')->nullable()->default(false);
            $table->boolean('hardcopy')->nullable()->default(false);
            $table->string('degree_image')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('user_employee_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('qualification_id')
                ->references('id')
                ->on('qualifications');

            $table->foreign('institute_id')
                ->references('id')
                ->on('institutes');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_education');
    }
}
