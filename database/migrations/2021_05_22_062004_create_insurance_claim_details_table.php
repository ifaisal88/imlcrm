<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceClaimDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_claim_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('insurance_claim_id');
            $table->unsignedBigInteger('insurance_benefit_id');
            $table->float('amount');
            $table->string('description')->nullable()->default(null);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_claim_details');
    }
}
