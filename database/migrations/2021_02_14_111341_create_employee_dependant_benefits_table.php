<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDependantBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_dependant_benefits', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('ticket_entitlement_id');
            $table->unsignedBigInteger('employee_dependant_id');
            $table->unsignedBigInteger('routes_from_id');
            $table->unsignedBigInteger('routes_to_id');
            $table->unsignedBigInteger('currency_id');
            $table->float('fare_amount', 8, 2);
            $table->unsignedBigInteger('insurance_company_id');
            $table->string('policy_number');
            $table->unsignedBigInteger('insurance_category_id');
            $table->float('premium', 8, 2);
            $table->float('insurance_amount', 8, 2);
            $table->date('start');
            $table->date('end');
            $table->boolean('status')->default(true);
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('ticket_entitlement_id')
                  ->references('id')
                  ->on('ticket_entitlements');

            $table->foreign('employee_dependant_id')
                  ->references('id')
                  ->on('employee_dependants');

            $table->foreign('routes_from_id')
                ->references('id')
                ->on('routes');

            $table->foreign('routes_to_id')
                ->references('id')
                ->on('routes');

            $table->foreign('insurance_company_id')
                ->references('id')
                ->on('insurance_companies');

            $table->foreign('insurance_category_id')
                ->references('id')
                ->on('insurance_categories');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_dependant_benefits');
    }
}
