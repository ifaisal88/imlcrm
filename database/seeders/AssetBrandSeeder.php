<?php

namespace Database\Seeders;


use Carbon\Carbon;
use App\Models\User;
use App\Models\AssetBrand;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $assetBrands = AssetBrand::all();
        if (count($assetBrands)==0) {
            DB::table('asset_brands')->insert([
                [
                    'name' => 'IBM',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'DELL',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
