<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\ContactPeople;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactPeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $contact_peoples = ContactPeople::all();
        if (count($contact_peoples)==0) {
            DB::table('contact_peoples')->insert([
                [

                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
