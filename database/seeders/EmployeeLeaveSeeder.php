<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\EmployeeLeave;
use App\Models\LeaveType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeLeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        $leave_type = LeaveType::first();
        $employee_leaves = EmployeeLeave::all();
        if (count($employee_leaves)==0) {
            DB::table('employee_leaves')->insert([
                [
                    'employee_id' => $user->employee_id,
                    'leave_type_id' => $leave_type->id,
                    'start_date' => now(),
                    'end_date' => now(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
