<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\InsuranceProvider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsuranceProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $insurance_providers = InsuranceProvider::all();
        if (count($insurance_providers)==0) {
            DB::table('insurance_providers')->insert([
                [
                    'name' => 'Insurance Provider 1',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Insurance Provider 2',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Insurance Provider 3',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
