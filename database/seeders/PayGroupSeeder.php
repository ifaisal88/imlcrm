<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\PayGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PayGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        $company = Company::where('name', 'Company 1')->first();
        $pay_groups = PayGroup::all();
        if (count($pay_groups)==0) {
            DB::table('pay_groups')->insert([
                [
                    'name' => 'Pay Group 1',
                    'company_id' => $company->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Pay Group 2',
                    'company_id' => $company->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
