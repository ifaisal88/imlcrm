<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();
        if (count($roles) == 0) {
            DB::table('roles')->insert([
                [
                    'name' => 'Admin',
                    'description' => 'This role is for admin.',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ],
                [
                    'name' => 'Employee',
                    'description' => 'This role is for the employee.',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
