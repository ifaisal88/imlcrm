<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\InsuranceCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsuranceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $insurance_categories = InsuranceCategory::all();
        if (count($insurance_categories)==0) {
            DB::table('insurance_categories')->insert([
                [
                    'name' => 'Insurance Category 1',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Insurance Category 2',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
