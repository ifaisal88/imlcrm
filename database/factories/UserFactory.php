<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;
        return [
            'full_name' => $name,
            'first_name' => explode(" ", $name)[0],
            'last_name' => explode(" ", $name)[1],
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'status' => true,
            'personal_id' => Str::random(10),
            'emirates_id' => Str::random(10),
            'labor_card_number' => Str::random(10),
            'person_code' => Str::random(10),
            'home_phone_number' => Str::random(10),
        ];
    }
}
